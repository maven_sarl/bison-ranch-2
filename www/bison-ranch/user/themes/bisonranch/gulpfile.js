require('events').EventEmitter.prototype._maxListeners = 1000;
process.env.DISABLE_NOTIFIER = true;

var elixir = require('laravel-elixir');
var gulp = require('gulp');
var iconfont = require('gulp-iconfont');
var iconfontCss = require('gulp-iconfont-css');
var replace = require('gulp-replace');
watch = require('gulp-watch');
var fontName = 'icon';

elixir.config.assetsPath = 'resources';
elixir.config.publicPath = '';

var tingpng = require('gulp-tinypng');

var images = [
    'images/**/*.{png,jpg}',
    '../../pages/**/*.{png,jpg}'
];

gulp.task('image', function () {
    gulp.src(images, {base: '.'})
    //.pipe(tingpng('REqM4CkY77zubkNaG-wIwz6DXMAHgibY')) // MAVEN
        .pipe(tingpng('NCuCCFIofwmtjZA-NpuOH1-SqY_FQSxV')) // E-nepsis
        .pipe(gulp.dest('.'));
});

/*gulp.task('icons', function(){
    gulp.src(['../maven/resources/icons/*.svg', 'resources/icons/*.svg'])
        .pipe(iconfontCss({
            fontName: fontName,
            //path: 'resources/sass/_icons.scss',
            formats: ['ttf', 'eot', 'woff'],
            targetPath: '_icons.scss',
            fontPath: '../fonts/icons/',
            normalize: true
        }))
        .pipe(iconfont({
            fontName: fontName
        }))
        .pipe(gulp.dest('fonts/icons'));
});*/

gulp.task('woff2', function(){
    gulp.src('fonts/icons/_icons.scss')
        .pipe(replace("woff2", "woff"))
        .pipe(gulp.dest('fonts/icons/'));
});



gulp.task('stream', function () {
    // Endless stream mode
    return watch('css/**/*.css', { ignoreInitial: false })
        .pipe(gulp.dest('build'));
});


elixir(function(mix) {
    mix
    //.task('woff2')
        .less(['main.less'], 'css/core.css');
});




