/*! www.bisonranch.ch - v1.0.0 - 2017-02-13 *//******************************@File: resources/assets/js/main.js******************************/

/*
(function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] 
                                   || window[vendors[x]+'CancelRequestAnimationFrame'];
    }
 
    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); }, 
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };
 
    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());

*/

var $header = {
	scrollLimit:400,
	obj:null,
	init:function(){
		if( document.body ){
			this.obj = document.body;
		}
	},
	animate:function( scrollY ){
		if(scrollY > ($("main section:first-of-type").offset().top - $(window).height()/2) ) {
	        $("body").attr("data-navigation_fixed", "true");
	    }else {
	    	$("body").attr("data-navigation_fixed", "false");
	    }
	}
}


$mobile = {
	init:function(){

		$("<div id='navigation-fixed'></div>").appendTo("body");
		$( "nav.navigation:first" ).clone().appendTo( "#navigation-fixed" );

		$("<div id='navigation-mobile'></div>").appendTo("body");
		$( "nav.navigation:first" ).clone().appendTo( "#navigation-mobile" );

		$(window).resize(function(){
			$("body").attr("data-navigation_mobile","false");
		});

		$("[id='mobile-menu-icon']").click(function(){
			if( $("body").attr("data-navigation_mobile") == "true" ){
				$("body").attr("data-navigation_mobile", "false");
			}else{
				$("body").attr("data-navigation_mobile","true");
			}
			return false;
		});

	}
}


$article = {
	init:function(){
		$("article").click(function(){
            if( $(this).find('a').length ){
                location.href = $(this).find('a:first-of-type').attr("href");
            }
			return false;
		});
	}
}


$(document).ready(function(){

	$article.init();
	$mobile.init();

	$(".gallery.photo").lightGallery();

	$header.init();

    //$main.loop();

    $(window).scroll(function(){
        $header.animate( $(window).scrollTop() );
    });

    $(".link-start").click(function(){
        var target = $(this).attr("data-target");
        //$music.fadeTo(0.2);
        $('html, body').animate({
            scrollTop: $(window).height()-100,
            easing:"easeInOutCubic"
        }, 2000);
        //$scroll.doIt();
        return false;
    });
/*
	setTimeout(function(){
		$("body").attr("data-domready", "true");
	}, 1000);
*/
});


