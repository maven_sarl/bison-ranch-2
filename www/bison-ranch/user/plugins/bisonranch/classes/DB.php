<?php
namespace Grav\Plugin\Bisonranch;

use PDO;

class DB
{

    private $PDOInstance = null;
    private static $instance = null;

    private function __construct($file)
    {
        $this->PDOInstance = new PDO($file);
    }

    public static function getInstance($file)
    {
        if(is_null(self::$instance))
        {
            self::$instance = new DB($file);
        }
        return self::$instance;
    }

    public function beginTransaction()
    {
        return $this->PDOInstance->beginTransaction();
    }

    public function commit()
    {
        return $this->PDOInstance->commit();
    }

    public function lastInsertId()
    {
        return $this->PDOInstance->lastInsertId();
    }

    public function prepare($query)
    {
        return $this->PDOInstance->prepare($query);
    }

    public function exec($query)
    {
        return $this->PDOInstance->exec($query);
    }

    public function query($query)
    {
        return $this->PDOInstance->query($query);
    }
}