<?php
namespace Grav\Plugin\Bisonranch;

use Grav\Common\Grav;
use Grav\Common\GravTrait;
use RocketTheme\Toolbox\File\File;
use Grav\Common\Page\Page;

use Grav\Plugin\Bisonranch\Controller;

class Contact extends Controller
{
    function route( $slim ){

        $self = $this;
        $slim->post($this->apiUrl . 'request-contact-send', function ($request, $response, $args) use ($self) {
            $errors = [];
            $data = $request->getParams();

            /*if( !$self->validateReCAPTCHA($data['g-recaptcha-response']) ){
                return $response->withJson(["error" => ["Recaptcha error"]], 500);
            }*/

            if (
                isset($data['email']) &&
                isset($data['comment']) &&
                isset($data['firstname']) &&
                isset($data['lastname']) &&
                filter_var($data['email'], FILTER_VALIDATE_EMAIL)
            ){
                $data['firstname']  = parent::test_input($data['firstname']);
                $data['lastname']   = parent::test_input($data['lastname']);
                $data['email']       = parent::test_input($data['email']);
            }else{
                return $response->withJson(["error" => ["Missing fields"]], 500);
            }

            if( $self->sendContact($data) ){
                if( !$self->sendContactAdmin($data) ) {
                    $errors[] = "Fail to send to admin";
                }
            }else{
                $errors[] = "Fail to send to user";
            }

            if( count($errors) != 0 ){
                return $response->withJson(["error" => $errors], 500);
            }
            return $response->withJson(["success" => true], 200);
        });

    }

    function sendContact( $form ){

        // CONFIG
        $from       = self::getGrav()['config']->get('plugins.email.email.from_email');
        $fromName   = self::getGrav()['config']->get('plugins.email.email.from_name');

        $vars = [
            "form" => $form,
            "page" => self::getGrav()['page'],
            "txt_01" => 'Merci d\'avoir pris contact avec Bison Ranch! Nous vous répondrons dès que possible.',
            "txt_02" => 'Pour rappel, voici les informations que vous nous avez communiquées:',
            "txt_03" => 'Cordialement,',
            "txt_04" => 'L\'équipe de Bison Ranch',
            "label_subject" => 'Concerne',
            "label_firstname" => 'Prénom',
            "label_lastname" => 'Nom',
            "label_email" => 'Email',
            "label_phone" => 'Téléphone',
            "label_comment" => 'Commentaire',
            "more_info" => 'En savoir plus',
            "heberegement" => [
                "dortoir" => [
                    "title" => "Dortoir",
                    "text" => "Passez une nuit seul ou à plusieurs dans l'un de nos dortoirs situés à l'auberge.",
                ],
                "cabane" => [
                    "title" => "Cabanes Far West",
                    "text" => "Séjournez en couple ou en famille dans une l'une de nos cabanes comme de vrais cowboys.",
                ],
                "tipi" => [
                    "title" => "Tipis indiens",
                    "text" => "Vivez l'expérience d'une nuit dans un authentique tipi indien.",
                ]
            ],
            "data" => [
                "theme" => [
                    "main" => "#ea690b",
                    "secondary" => "#00438a",
                    "width" => 600
                ]
            ]
        ];

        $subject = self::getGrav()['language']->translate(["CONTACT.EMAIL.SUBJECT"]);

        $content = "";
        self::getGrav()['twig']->twig_paths[] = __DIR__ . '/templates';
        $content = self::getGrav()['twig']->processString('{% include "emails/thanks.html.twig" %}', $vars);

        $message = self::getGrav()['Email']->message($subject, $content, 'text/html')
            ->setFrom($from, $fromName)
            ->setTo($form["email"]);

        return self::getGrav()['Email']->send($message);
    }


    function sendContactAdmin( $form ){
        // CONFIG
        $from       = $form["email"];
        $fromName   = $form["firstname"] . " " . $form["lastname"];

        $to       = self::getGrav()['config']->get('plugins.email.email.to_email');
        $toBcc    = self::getGrav()['config']->get('plugins.email.email.to_bcc_email');
        $toName   = self::getGrav()['config']->get('plugins.email.email.to_name');


        $subject = self::getGrav()['language']->translate(["CONTACT.EMAIL.SUBJECT"]);

        $comment = nl2br($form["comment"]);
        $comment = str_replace("\r","", $comment);
        $comment = str_replace("\n","", $comment);

        $content = $comment;

        $content .= "<br><hr><br><table>";
        $content .= "<tr><td>Nom:</td><td>".$form["firstname"]."</td></tr>";
        $content .= "<tr><td>Prénom:</td><td>".$form["lastname"]."</td></tr>";
        $content .= "<tr><td>Téléphone:</td><td>".$form["phone"]."</td></tr>";
        $content .= "<tr><td>Email:</td><td>".$form["email"]."</td></tr>";
        $content .= "<tr><td>Sujet:</td><td>".$form["subject"]."</td></tr>";
        $content .= "</table>";

        $message = self::getGrav()['Email']->message($subject, $content, 'text/html')
            ->setTo($to, $toName)
            ->setBcc($toBcc)
            ->setReplyTo($from)
            ->setFrom($from, $fromName);

        return self::getGrav()['Email']->send($message);
    }



}