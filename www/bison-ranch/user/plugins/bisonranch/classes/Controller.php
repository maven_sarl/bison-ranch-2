<?php
namespace Grav\Plugin\Bisonranch;

use Grav\Common\Grav;
use RocketTheme\Toolbox\File\File;
use Grav\Common\Plugin;
use Grav\Common\User\User;
use Grav\Common\Page\Page;
use Grav\Common\Page\Pages;
use Grav\Plugin\Bisonranch\DB;

use Grav\Common\GravTrait;

class Controller
{

    use GravTrait;

    protected $language;
    protected $theme;
    protected $uri;
    protected $db;

    function __construct(){

        $this->language     = self::getGrav()['language']->getActive()?self::getGrav()['language']->getActive():self::getGrav()['theme']->config()["default_lang"];
        $this->uri          = self::getGrav()['uri'];
        $this->apiUrl       = "/" . $this->language . "/" . self::getGrav()['config']->get('plugins.bisonranch.apiUrl') ."/";
        $this->theme        = self::getGrav()['themes']->current();

        // DB
        $path               = self::getGrav()['locator']->findResource('user://data', true);
        $database           = $path . DS . "bisonranch.sqlite";
        $this->db           = DB::getInstance('sqlite:' . $database);

    }

    public function DB(){
        return $this->db;
    }

    public static function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    public function validateReCAPTCHA( $postedKey ){

        $secretKey = self::getGrav()['config']->get('site.google_recaptcha.secret_key');

        if( isset($postedKey) && !empty($postedKey) ){

            $grecaptcha = $postedKey;
            $secretkey  = $secretKey;

            $params = array(
                'secret' => $secretkey,
                'response' => $grecaptcha
            );

            $url = "https://www.google.com/recaptcha/api/siteverify?" . http_build_query($params);

            if (function_exists('curl_version')) {
                $curl = curl_init($url);
                curl_setopt($curl, CURLOPT_HEADER, false);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_TIMEOUT, 5);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                $res = curl_exec($curl);
            } else {
                // Si curl n'est pas dispo, un bon vieux file_get_contents
                $res = file_get_contents($url);
            }


            $json = json_decode($res);

            //$response   = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secretkey."&response=".$grecaptcha), true);
            return $json->success?true:false;

        }

        return false;

    }

}