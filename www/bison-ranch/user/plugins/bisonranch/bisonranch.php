<?php
namespace Grav\Plugin;

use Grav\Common\Plugin;
use RocketTheme\Toolbox\Event\Event;

use Grav\Plugin\Bisonranch\Contact;
use Grav\Plugin\Bisonranch\Calculate;

/**
 * Class BisonranchPlugin
 * @package Grav\Plugin
 */
class BisonranchPlugin extends Plugin
{
    /**
     * @return array
     *
     * The getSubscribedEvents() gives the core a list of events
     *     that the plugin wants to listen to. The key of each
     *     array section is the event that the plugin listens to
     *     and the value (in the form of an array) contains the
     *     callable (or function) as well as the priority. The
     *     higher the number the higher the priority.
     */
    public static function getSubscribedEvents()
    {
        return [
            'onPluginsInitialized' => ['onPluginsInitialized', 0],
            'onTwigTemplatePaths'    => ['onTwigTemplatePaths', 0]
        ];
    }

    /**
     * Initialize the plugin
     */
    public function onPluginsInitialized()
    {

        require_once __DIR__.'/vendor/autoload.php';


        if ($this->isAdmin()) {
            $this->active = false;

            $this->enable([
                'onTwigTemplatePaths' => ['onTwigTemplatePaths', 0],
                'onPageInitialized' => ['onPageAdminInitialized', 0],
                'onAdminMenu' => ['onAdminMenu', 0]
            ]);

            return;
        }

        $this->enable([
            'onTwigTemplatePaths' => ['onTwigTemplatePaths', 0],
            'onTwigSiteVariables' => ['onTwigSiteVariables', 0],
            'onPageInitialized' => ['onPageInitialized', 0]
        ]);
    }

    /**
     * Add current directory to twig lookup paths.
     */
    public function onTwigTemplatePaths()
    {
        if (!$this->isAdmin()) {
            $this->grav['twig']->twig_paths[] = __DIR__ . '/templates';
        }else{
            //$this->grav['twig']->twig_paths[] = __DIR__ . '/admin/templates';
        }
    }

    /**
     * Set needed variables to display pagination.
     */
    public function onTwigSiteVariables()
    {
        /*
        $this->grav['assets']
            ->add('jquery', 101)
            ->addJs('plugin://maven/js/angular.min.js')
            ->addJs('plugin://maven/js/app.js')
            ->addJs('plugin://maven/js/controllers/contactController.js')
            ->addJs('plugin://maven/js/controllers/orderController.js')
            ->addJs('plugin://maven/js/controllers/donationController.js')
            ->addJs('plugin://maven/plugins/lightgallery/js/lightgallery.min.js')
            ->addCss('plugin://maven/plugins/lightgallery/css/lightgallery.min.css');
        */
    }

    /**
     * Do some work for this event, full details of events can be found
     * on the learn site: http://learn.getgrav.org/plugins/event-hooks
     *
     * @param Event $e
     */
    public function onPageContentRaw(Event $e)
    {
        // Get a variable from the plugin configuration
        //$text = $this->grav['config']->get('plugins.bisonranch.text_var');

        // Get the current raw content
        //$content = $e['page']->getRawContent();

        // Prepend the output with the custom text and set back on the page
        //$e['page']->setRawContent($text . "\n\n" . $content);
    }

    public function onPageAdminInitialized()
    {

    }

    public function onPageInitialized()
    {

        $uri    = $this->grav['uri'];
        $config = $this->grav['config'];

        if( preg_match("#[/]?".$config->get("plugins.bisonranch.apiUrl")."/(.*)#i", $uri->path()) ){
            $this->routePage();
            exit();
        }

        if( preg_match("#[/]?cron#i", $uri->path()) ){
            $Calculate  = new Calculate();
            $Calculate->cron();
            exit();
        }

    }


    private function routePage()
    {

        // Create and configure Slim app
        $config = ['settings' => [
            'displayErrorDetails' => true,
            'addContentLengthHeader' => false,
            'debug' => true
        ]];

        $c = new \Slim\Container( $config ); //Create Your container

        //Override the default Not Found Handler
        $c['notFoundHandler'] = function ($c) {
            return function ($request, $response) use ($c) {
                return $c['response']
                    ->withJson(['error' => 'Page not found'], 404);
            };
        };
        $c['notAllowedHandler'] = function ($c) {
            return function ($request, $response, $methods) use ($c) {
                return $c['response']
                    ->withJson(['error' => 'Method must be one of: ' . implode(', ', $methods)], 405);
            };
        };
        $c['errorHandler'] = function ($c) {
            return function ($request, $response, $exception) use ($c) {
                return $c['response']
                    ->withJson(['error' => $exception->getMessage()], 500);
            };
        };

        $slim = new \Slim\App($c);

        $Contact  = new Contact();
        $Contact->route( $slim );

        // Run app
        $slim->run();

    }


    public function onAdminMenu()
    {

    }
}
