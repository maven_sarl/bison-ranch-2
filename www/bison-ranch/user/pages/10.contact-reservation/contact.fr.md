---
title: 'CONTACT ET RÉSERVATION'
media_order: wallpaper_homepage.jpg
meta_title: 'Nous contacter'
meta_description: 'Réservation - Horaires - Accès'
background_image: wallpaper_homepage.jpg
access_content: "**En voiture**\r\n\r\nSuivre le plan GoogleMap. En hiver, se renseigner sur les [conditions d'enneigement](http://snow.myswitzerland.com/bulletin_enneigement/JuraTroisLacs/LesPresdOrvinChasseral-206).\r\n\r\n**En train**\r\n\r\nDepuis la gare de Bienne, ensuite bus postal depuis la gare jusqu'aux Prés d'Orvin (4 bus par jour-changement des horaires en semaine et le week-end), ensuite 1h de marche.\r\n\r\nDepuis la gare de La Neuveville, ensuite bus postal jusqu'à Nods, ensuite 1h15 de marche.\r\nLiaison Nods - Chasseral : sur réservation au 032 751 64 89, ensuite 2h10 de marche, direction EST par la crête."
slug: contact
---

Réservation - Horaires - Accès