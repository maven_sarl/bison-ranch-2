---
title: 'KONTAKT UND RESERVIERUNG'
media_order: wallpaper_homepage.jpg
meta_title: 'Uns kontaktieren'
meta_description: 'Reservierung - Öffnungszeiten - Anfahrt'
background_image: wallpaper_homepage.jpg
access_content: "**Mit dem Auto**\r\n\r\nFolgen Sie der GoogleMaps Karte. Informieren Sie sich über die [Schneebedingungenim](http://snow.myswitzerland.com/bulletin_enneigement/JuraTroisLacs/LesPresdOrvinChasseral-206) Winter.\r\n\r\n**Mit dem Zug**\r\n\r\nbis zum Bahnhof Biel, anschliessend mit dem Bus nach Prés d'Orvin (4 Busse pro Tag / Änderung der Fahrzeiten unter der Woche und am Wochenende), dann 1h Fussmarsch.\r\n\r\nbis zum Bahnhof La Neuveville, anschliessend mit dem Bus nach Nods, dann 1h15 Fussmarsch.\r\nVerbindung Nods - Chasseral: auf Reservierung unter 032 751 64 89, dann 2h10 zu Fuss in Richtung Ost des Gebirgskamms."
slug: kontakt
---

Reservierung - Öffnungszeiten - Anfahrt