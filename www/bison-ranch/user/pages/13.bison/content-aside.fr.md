---
title: 'Parc à bison'
media_order: 'bison_indien.jpg,bison-americain.png,bison-europeen.png,bison-naissance.gif,bison03.jpg,wallpaper_bison.jpg,wallpaper_homepage.jpg'
meta_title: 'Parc à bison'
meta_description: 'Des grands, des petits, des barbus'
background_image: wallpaper_bison.jpg
contents:
    -
        id: bison-elevage
        class: block
        contents:
            -
                title: 'ELEVAGE DE BISONS AU RANCH'
                image: bison03.jpg
            -
                content: "**Voilà plus de 28 ans que Christian Lecomte s'est lancé dans l'élevage particulier de** bison des plaines nord-américaines.\r\n\r\n**Accueillant depuis une vingtaine d'années des vaches et des génisses en estivage, le domaine des Colisses du Bas avait besoin d'une diversification de sa production.**\r\n\r\nC'est ainsi qu'en mai 1992, cinq bisons femelles arrivèrent du Dakota du sud, rejointes en 1993 par un mâle. Le troupeau s'agrandit durant l'hiver 94-95 avec l'arrivée de six jeunes (deux mâles et quatres femelles) provenant de Belgique mais de parents américains."
                image: bison-naissance.gif
            -
                content: "Les premières naissances eurent lieu fin juin début juillet 1995. L'élevage était lancé.\r\n\r\nAujourd'hui, l'élevage comprend une cinquentaine de bisons, dont une quinzaine de naissances chaque année."
    -
        id: bison-about
        class: block
        contents:
            -
                column_1_content: "<h2>A propos du bison</h2>\r\n<p><a href=\"http://fr.wikipedia.org/wiki/Bison\" target=\"_blank\">Le bison</a>, symbole des grandes plaines américaines, était la base de vie des tribus indiennes des plaines.</p>\r\n<p>D’après les premiers explorateurs du 16ème siècle, le Nouveau Monde devait abriter près de 60 millions de bisons, la plus grande concentration de grands animaux que la terre n’ait jamais connue. Dans les années 1890, ils n’étaient plus que quelques centaines. Les colons les avaient exterminés pour soumettre les Indiens afin d’acquérir leur terre et laisser la place à la civilisation de l’homme blanc.</p>\r\n"
                column_2_content: '<figure markdown="1">![Historique du bison](bison_indien.jpg)<figcaption>Source Google</figcaption></figure>'
            -
                column_1_content: "<h3>Les différentes espèces</h3>\r\n<p>Il Existe 2 principales espèces de bison: <a href=\"#\" target=\"_blank\">Le bison d'Europe</a> et <a href=\"#\" target=\"_blank\">le bison américain</a></p>\r\n<p>Le bison d’Europe vit dans la forêt. Disparu en 1942, il fut réintroduit en 1956 dans les forêts polonaises à partir de bêtes de zoo.</p>\r\n<p>Le bison américain est divisé 2 sous-espèces :</p>\r\n<ul>\r\n<li>Le bison des bois qui vit dans les forêts du nord-ouest canadien.</li>\r\n<li>Le bison des plaines que l’on trouve dans les grandes plaines américaines et les parcs nationaux.</li>\r\n</ul>\r\n<p>Le bison des plaines se prête le mieux à l’élevage et qui aujourd’hui est importé en Europe. De la famille des bovidés, il peut atteindre 1,80 m. au garrot pour le mâle et peser jusqu'à 1,3 tonne à l’âge adulte.</p>\r\n<p>Les femelles sont nettement moins grandes et moins lourdes de 25 à 50 %. Le bison peut vivre jusqu'à 25 ans. Le rut dure de juillet à septembre pour une gestation de 9 mois. A la naissance, le bisonneau pèse de 20 à 30 kg et a un pelage roux qui virera au brun vers la dixième semaine</p>\r\n"
                column_2_content: "<figure markdown=\"1\">![especes de bisons](bison-americain.png)<figcaption>Bison américain</figcaption></figure>\r\n<figure markdown=\"1\">![especes de bisons](bison-europeen.png)<figcaption>Bison d'Europe</figcaption></figure>"
---

Des grands, des petits, des barbus