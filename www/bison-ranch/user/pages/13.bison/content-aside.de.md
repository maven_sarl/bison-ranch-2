---
title: BISONPARK
media_order: 'bison_indien.jpg,bison-americain.png,bison-europeen.png,bison-naissance.gif,bison03.jpg,wallpaper_bison.jpg,wallpaper_homepage.jpg'
meta_title: Bisonpark
meta_description: 'Grosse, kleine, bärtige'
background_image: wallpaper_bison.jpg
contents:
    -
        id: bison-elevage
        class: block
        contents:
            -
                title: 'Bisonzucht auf der Ranch'
                image: bison03.jpg
            -
                content: "**Vor über 28 Jahren hat Christian Lecomte mit der Zucht von** nordamerikanischen Präriebisons begonnen.\r\n\r\n**Während dieser Zeit wurden Kühe und Färsen zur Sömmerung aufgenommen, da das Gebiet Coulisses du Bas eine Diversifikation der Produktion benötigte.**\r\n\r\nSo wurden im Mai 1992 fünf weibliche Bisons aus South Dakota importiert. 1993 kam ein männlicher Bison dazu. Die Herde vergrösserte sich im Winter 94/95 mit der Ankunft von sechs Jungtieren (zwei Männchen und vier Weibchen) aus Belgien, aber mit amerikanischen Eltern."
                image: bison-naissance.gif
            -
                content: "Die ersten Geburten fanden Ende Juni Anfang Juli 1995 statt. Die Zucht hatte begonnen.\r\n\r\nHeute besteht die Zucht aus ca. 50 Bisons, darunter 15 Geburten pro Jahr."
    -
        id: bison-about
        class: block
        contents:
            -
                column_1_content: "<h2>Über die Bisons</h2>\r\n<p><a href=\"http://fr.wikipedia.org/wiki/Bison\" target=\"_blank\">Der Bison</a>, Symbol der amerikanischen Great Plains, war die Lebensgrundlage der indianischen Präriestämme.</p>\r\n<p>Nach den ersten Entdeckern des 16. Jahrhunderts zufolge, bot die Neue Welt etwa 60 Mio. Bisons Schutz, die grösste Konzentration von Grosstieren, die die Erde jemals gesehen hatte. In den 1890er Jahren waren es nur noch ein paar hundert. Die Siedler hatten sie ausgerottet um die Indianer zu unterwerfen, deren Land zu erwerben und den Weg für die Zivilisation des weissen Mannes frei zu machen.</p>\r\n"
                column_2_content: '<figure markdown="1">![Historique du bison](bison_indien.jpg)<figcaption>Quelle Google</figcaption></figure>'
            -
                column_1_content: "<h3>Die verschiedenen Bisonarten</h3>\r\n<p>Es existieren hauptsächlich 2 Arten: <a href=\"#\" target=\"_blank\">Der Wisent</a> und <a href=\"#\" target=\"_blank\">der Amerikanische Bison</a></p>\r\n<p>Der Wisent (Europäische Bison) lebt im Wald. Er verschwand im Jahr 1942 und wurde 1956 in polnischen Wäldern mit Hilfe von Zootieren wieder eingeführt.</p>\r\n<p>Der Amerikanische Bison teilt sich in zwei Unterarten:</p>\r\n<ul>\r\n<li>Der Waldbison, der in den Wäldern im Nordwesten von Kanada lebt.</li>\r\n<li>Der Präriebison, den man in den amerikanischen Flachländern und Nationalparks findet.</li>\r\n</ul>\r\n<p>Letzterer eignet sich am besten für die Zucht und ist heute in Europa eingeführt. Er gehört zur Familie der wiederkäuenden Paarhufer. Das Männchen wird am Widerrist bis zu 1,80m gross und wiegt ausgewachsen bis zu 1,3 Tonnen.</p>\r\n<p>Die Weibchen sind deutlich kleiner und wiegen 25 bis 50% weniger. Der Bison kann bis zu 25 Jahre alt werden. Die Brunftzeit ist von Juli bis September, die Tragezeit beträgt 9 Monate. Bei der Geburt wiegt ein Bison zwischen 20 und 30kg und hat ein rötliches Fell, das sich ab der zehnten Woche braun färbt.</p>"
                column_2_content: "<figure markdown=\"1\">![especes de bisons](bison-americain.png)<figcaption>Amerikanischer Bison</figcaption></figure>\r\n<figure markdown=\"1\">![especes de bisons](bison-europeen.png)<figcaption>Wisent (Europäischer Bison)</figcaption></figure>"
---

Grosse, kleine, bärtige