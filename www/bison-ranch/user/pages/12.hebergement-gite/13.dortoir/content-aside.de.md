---
title: 'SCHLAFSÄLE IM GASTHAUS'
media_order: 'gite-dortoir_00.jpg,gite-dortoir_01.jpg,gite-dortoir_02.jpg,gite-dortoir.jpg,wallpaper_gite_dortoir.jpg,gite-cabane.jpg,gite-tipi.jpg'
meta_title: 'Schlafsäle im Gasthaus'
meta_description: 'Verbringen Sie eine Nacht alleine oder zu mehreren in einem unserer Schlafsäle'
background_image: wallpaper_gite_dortoir.jpg
contents:
    -
        id: gite-dortoir
        class: block
        aside:
            -
                content: "<div class=\"information\">\r\n        <p>Reservierungen sind möglich von Mittwoch bis Sonntag. Wir haben montags und dienstags geschlossen.</p>\r\n\r\n        <ul>\r\n            <li>Ankunft: <i>15h30</i></li>\r\n            <li>Abreise: <i>11h</i></li>\r\n            <li>Frühstück: <i>8h30 bis 10h30</i></li>\r\n        </ul>\r\n </div>\r\n<nav class=\"subnavigation\">\r\n    <ul>\r\n        <li markdown=\"1\">[Reservierung <i>Kontaktformular</i>](/contact)</li>\r\n        <li markdown=\"1\">[Anfahrtsplan <i>Wo Sie uns finden</i>](/contact#access)</li>\r\n    </ul>\r\n</nav>"
        contents:
            -
                title: 'UNSERE SCHLAFSÄLE'
                content: "**Das Gasthaus der Bison Ranch bietet Ihnen zwei komfortable Schlafsäle für einzelne Personen, Familien oder Gruppen.**\r\n\r\nDer kleine Schlafsaal besteht aus 9 Schlafplätzen, der grosse zählt 12 Betten."
            -
                title: PREISE
                content: "<div class=\"tarif\">\r\n    <dl>\r\n        <dt>2 - 10 Jahre</dt>\r\n        <dd>20<i>chf</i></dd>\r\n    </dl>\r\n    <dl>\r\n        <dt>10 - 16 Jahre</dt>\r\n        <dd>30<i>chf</i></dd>\r\n    </dl>\r\n    <dl>\r\n        <dt>Erwachsene</dt>\r\n        <dd>40<i>chf</i></dd>\r\n    </dl>\r\n    <p class=\"hint\">Gäste, die nicht im Restaurant essen, müssen einen Aufpreis von 5.- pro Nacht zahlen.</p>\r\n</div>\r\n<p>Der Übernachtungspreis beinhaltet:</p>\r\n<ul>\r\n    <li>Eine Matratze und eine Decke *</li>\r\n    <li>Ein Frühstück</li>\r\n    <li>Toiletten und Gemeinschaftsduschen</li>\r\n    <li>Kostenlos für Kinder unter 2 Jahren</li>\r\n</ul>\r\n<p>* Wir empfehlen Ihnen, für Ihr Wohlbefinden, sich mit einem Schlafsack zu wappnen, zusätzlich zu den Decken, die Ihnen zur Verfügung stehen.</p>"
            -
                title: FOTO-GALERIE
                gallery:
                    -
                        image: gite-dortoir.jpg
                    -
                        image: gite-dortoir_00.jpg
                    -
                        image: gite-dortoir_01.jpg
                    -
                        image: gite-dortoir_02.jpg
    -
        id: gite-dortoir-aside
        class: block
        contents:
            -
                title: 'ANDERE UNTERKÜNFTE'
                content: "<ul class=\"list\">\r\n    <li>\r\n        <article>\r\n            <figure markdown=\"1\">![Hütte](gite-cabane.jpg)</figure>\r\n            <h2 class=\"title\"><a href=\"/unterkunft/hutten-wilder-western\">Hütten Wilder Westen</a></h2>\r\n            <span class=\"category\">Unterkunft</span>\r\n            <p>Wohnen Sie als Paar oder als Familie in einer unserer Hütten wie echte Cowboys.</p>\r\n            <button class=\"more\"><span>Mehr erfahren</span></button>\r\n        </article>\r\n    </li>\r\n    <li>\r\n        <article>\r\n            <figure markdown=\"1\">![Tipi](gite-tipi.jpg)</figure>\r\n            <h2 class=\"title\"><a href=\"/unterkunft/indianer-tipis\">Indianer-Tipis</a></h2>\r\n            <span class=\"category\">Unterkunft</span>\r\n            <p>Erleben Sie eine Nacht in einem echten indianischen Tipi.</p>\r\n            <button class=\"more\"><span>Mehr erfahren</span></button>\r\n        </article>\r\n    </li>\r\n</ul>"
slug: schlafsale-gasthaus
---

Verbringen Sie eine Nacht alleine oder zu mehreren in einem unserer Schlafsäle