---
title: 'DORTOIRS À L''AUBERGE'
media_order: 'gite-dortoir_00.jpg,gite-dortoir_01.jpg,gite-dortoir_02.jpg,gite-dortoir.jpg,wallpaper_gite_dortoir.jpg,gite-cabane.jpg,gite-tipi.jpg'
meta_title: 'Dortoirs à l''auberge'
meta_description: 'Passez une nuit seul ou à plusieurs dans l''un de nos dortoirs'
background_image: wallpaper_gite_dortoir.jpg
contents:
    -
        id: gite-dortoir
        class: block
        aside:
            -
                content: "<div class=\"information\">\r\n    <p>Les réservations sont possibles du mercredi au dimanche. Nous sommes fermés le lundi et mardi.</p>\r\n    <ul>\r\n        <li>Heure d'arrivée : <i>15h30</i></li>\r\n        <li>Heure de départ : <i>11h</i></li>\r\n        <li>Petit déjeuner : <i>8h30 à 10h30</i></li>\r\n    </ul>\r\n</div>\r\n<nav class=\"subnavigation\">\r\n    <ul>\r\n        <li markdown=\"1\">[Réservation <i>Formulaire de contact</i>](/contact)</li>\r\n        <li markdown=\"1\">[Plan d'accès <i>Ou nous trouver ?</i>](/contact#access)</li>\r\n    </ul>\r\n</nav>"
        contents:
            -
                title: 'Nos dortoirs'
                content: "**L'Auberge de Bison Ranch vous propose deux dortoirs confortables pour personnes seules, familles ou groupes.**\r\n\r\nLe dortoir le plus petit comporte 9 places, le plus grand comporte quant à lui 12 places."
            -
                title: Tarifs
                content: "<div class=\"tarif\">\r\n    <dl>\r\n        <dt>2 à 10ans</dt>\r\n        <dd>20<i>chf</i></dd>\r\n    </dl>\r\n    <dl>\r\n        <dt>10 - 16ans</dt>\r\n        <dd>30<i>chf</i></dd>\r\n    </dl>\r\n    <dl>\r\n        <dt>Adulte</dt>\r\n        <dd>40<i>chf</i></dd>\r\n    </dl>\r\n    <p class=\"hint\">Les personnes ne mangeant pas au restaurant devront s'acquitter d'un supplément de 5.-/par nuit.</p>\r\n</div>\r\n<p>Le prix de la nuitée inclut:</p>\r\n<ul>\r\n    <li>Un matelas et une couverture</li>\r\n    <li>Un petit déjeuner</li>\r\n    <li>Toilettes et douches communes</li>\r\n    <li>Gratuit pour les enfants de moins de 2ans</li>\r\n</ul>\r\n<p>* Il est vivement conseillé, pour votre confort, de vous munir de votre sac de couchage, malgré nos couvertures à disposition.</p>"
            -
                title: 'GALERIE DE PHOTOS'
                gallery:
                    -
                        image: gite-dortoir.jpg
                    -
                        image: gite-dortoir_00.jpg
                    -
                        image: gite-dortoir_01.jpg
                    -
                        image: gite-dortoir_02.jpg
    -
        id: gite-dortoir-aside
        class: block
        contents:
            -
                title: 'AUTRES LOGEMENTS À DISPOSITION'
                content: "<ul class=\"list\">\r\n    <li>\r\n        <article>\r\n            <figure markdown=\"1\">![](gite-cabane.jpg)</figure>\r\n            <h2 class=\"title\"><a href=\"/hebergement-gite/cabane\">Cabanes Far West</a></h2>\r\n            <span class=\"category\">Gîte</span>\r\n            <p>Séjournez en couple ou en famille dans une l'une de nos cabanes comme de vrais cowboys.</p>\r\n            <button class=\"more\"><span>En savoir plus</span></button>\r\n        </article>\r\n    </li>\r\n    <li>\r\n        <article>\r\n            <figure markdown=\"1\">![tipi](gite-tipi.jpg)</figure>\r\n            <h2 class=\"title\"><a href=\"/hebergement-gite/tipi\">Tipis indiens</a></h2>\r\n            <span class=\"category\">Gîte</span>\r\n            <p>Vivez l'expérience d'une nuit dans un authentique tipi indien.</p>\r\n            <button class=\"more\"><span>En savoir plus</span></button>\r\n        </article>\r\n    </li>\r\n</ul>"
---

Passez une nuit seul ou à plusieurs dans l'un de nos dortoirs