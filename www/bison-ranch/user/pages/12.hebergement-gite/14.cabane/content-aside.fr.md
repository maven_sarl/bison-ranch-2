---
title: 'Cabanes far west'
media_order: 'gite-cabane_00.jpg,gite-cabane_01.jpg,gite-cabane_02.jpg,gite-cabane_03.jpg,gite-cabane_04.jpg,gite-cabane_05.jpg,gite-cabane_06.jpg,gite-cabane_07.jpg,gite-cabane.jpg,gite-dortoir.jpg,gite-tipi.jpg,wallpaper_gite_cabane.jpg'
meta_title: 'Cabane Far West'
meta_description: 'Un séjour inoubliable comme de vrais cowboys'
background_image: wallpaper_gite_cabane.jpg
contents:
    -
        id: gite-cabane
        class: block
        aside:
            -
                content: "<div class=\"information\">\r\n    <p>Les réservations sont possibles du mercredi au dimanche. Nous sommes fermés le lundi et mardi.</p>\r\n    <ul>\r\n        <li>Heure d'arrivée : <i>15h30</i></li>\r\n        <li>Heure de départ : <i>11h</i></li>\r\n        <li>Petit déjeuner : <i>8h30 à 10h30</i></li>\r\n    </ul>\r\n</div>\r\n<nav class=\"subnavigation\">\r\n    <ul>\r\n        <li markdown=\"1\">[Réservation <i>Formulaire de contact</i>](/contact)</li>\r\n        <li markdown=\"1\">[Plan d'accès <i>Ou nous trouver ?</i>](/contact#access)</li>\r\n    </ul>\r\n</nav>"
        contents:
            -
                title: 'NOS CABANES FAR WEST'
                content: "<p class=\"preamble\">Pour les cowboys amateurs, le Bison Ranch vous propose plusieurs cabanes de 2 à 4 places pour couples, familles ou petits groupes. 2 landaus (120x60cm) sont disponibles pour les bébés.</p>\r\n<p>Nos cabanes sont toutes munies d'un poêle à bois, d'un lit double et de lits superposés pour les plus grandes cabanes. Les <a href=\"/hebergement-gite/toilettes-seches\">toilettes sèches</a> communes sont disponibles à l'extérieur des bâtisses.</p>\r\n"
            -
                title: Tarifs
                content: "<div class=\"tarif\">\r\n        <dl>\r\n            <dt>2 à 10ans</dt>\r\n            <dd>20<i>chf</i></dd>\r\n        </dl>\r\n        <dl>\r\n            <dt>10 - 16ans</dt>\r\n            <dd>35<i>chf</i></dd>\r\n        </dl>\r\n        <dl>\r\n            <dt>Adulte</dt>\r\n            <dd>55<i>chf</i></dd>\r\n        </dl>\r\n        <p class=\"hint\">Les personnes ne mangeant pas au restaurant devront s'acquitter d'un supplément de 5.-/par nuit.</p>\r\n    <p class=\"hint\">Réduction dès la 2ème nuit.</p>\r\n</div>\r\n<p>Le prix de la nuitée inclut:</p>\r\n<ul>\r\n    <li>Un matelas et une couverture *</li>\r\n    <li>Un petit déjeuner</li>\r\n    <li>Bois non fourni, peut être acheté sur place</li>\r\n    <li>Des lanternes pour s'éclairer</li>\r\n    <li>Toilettes sèches communes à l'extérieur</li>\r\n    <li>Gratuit pour les enfants de moins de 2ans</li>\r\n</ul>\r\n<p>* Il est vivement conseillé, pour votre confort, de vous munir de votre sac de couchage, malgré nos couvertures à disposition.</p>\r\n<p>Le premier sac de bois est offert à partir de deux achetés (9.-/sac)</p>"
            -
                title: 'GALERIE DE PHOTOS'
                gallery:
                    -
                        image: gite-cabane.jpg
                    -
                        image: gite-cabane_00.jpg
                    -
                        image: gite-cabane_01.jpg
                    -
                        image: gite-cabane_02.jpg
                    -
                        image: gite-cabane_03.jpg
                    -
                        image: gite-cabane_04.jpg
                    -
                        image: gite-cabane_05.jpg
                    -
                        image: gite-cabane_06.jpg
                    -
                        image: gite-cabane_07.jpg
    -
        id: gite-cabane-aside
        class: block
        contents:
            -
                title: 'AUTRES LOGEMENTS À DISPOSITION'
                content: "<ul class=\"list\">\r\n    <li>\r\n        <article>\r\n            <figure markdown=\"1\">![](gite-dortoir.jpg)</figure>\r\n            <h2 class=\"title\"><a href=\"/hebergement-gite/dortoir\">Dortoirs</a></h2>\r\n            <span class=\"category\">Hébergement</span>\r\n            <p>Passez une nuit seul ou à plusieurs dans l'un de nos dortoirs situés à l'auberge.</p>\r\n            <button class=\"more\"><span>En savoir plus</span></button>\r\n        </article>\r\n    </li>\r\n    <li>\r\n        <article>\r\n            <figure markdown=\"1\">![](gite-tipi.jpg)</figure>\r\n            <h2 class=\"title\"><a href=\"/hebergement-gite/tipi\">Tipis indiens</a></h2>\r\n            <span class=\"category\">Gîte</span>\r\n            <p>Vivez l'expérience d'une nuit dans un authentique tipi indien.</p>\r\n            <button class=\"more\"><span>En savoir plus</span></button>\r\n        </article>\r\n    </li>\r\n</ul>"
---

Un séjour inoubliable comme de vrais cowboys