---
title: 'HÜTTEN WILDER WESTEN'
media_order: 'gite-cabane_00.jpg,gite-cabane_01.jpg,gite-cabane_02.jpg,gite-cabane_03.jpg,gite-cabane_04.jpg,gite-cabane_05.jpg,gite-cabane_06.jpg,gite-cabane_07.jpg,gite-cabane.jpg,gite-dortoir.jpg,gite-tipi.jpg,wallpaper_gite_cabane.jpg'
meta_title: 'Hütten Wilder Western'
meta_description: 'Ein unvergesslicher Aufenthalt wie echte Cowboys'
background_image: wallpaper_gite_cabane.jpg
contents:
    -
        id: gite-cabane
        class: block
        aside:
            -
                content: "<div class=\"information\">\r\n        <p>Reservierungen sind möglich von Mittwoch bis Sonntag. Wir haben montags und dienstags geschlossen.</p>\r\n\r\n        <ul>\r\n            <li>Ankunft: <i>15h30</i></li>\r\n            <li>Abreise: <i>11h</i></li>\r\n            <li>Frühstück: <i>8h30 bis 10h30</i></li>\r\n        </ul>\r\n </div>\r\n<nav class=\"subnavigation\">\r\n    <ul>\r\n        <li markdown=\"1\">[Reservierung <i>Kontaktformular</i>](/contact)</li>\r\n        <li markdown=\"1\">[Anfahrtsplan <i>Wo Sie uns finden</i>](/contact#access)</li>\r\n    </ul>\r\n</nav>"
        contents:
            -
                title: 'NOS CABANES FAR WEST'
                content: "<p class=\"preamble\">Für Hobby-Cowboys bietet die Bison Ranch mehrere Hütten mit 2 bis 4 Schlafplätzen, für Paare, Familien oder kleine Gruppen. Für Babies stehen 2 Kinderbetten (120x60cm) zur Verfügung.</p>\r\n<p>Unsere Hütten sind alle mit einem Holzofen ausgestattet, sowie einem Doppelbett und einem Hochbett in den grösseren. Gemeinschaftliche <a href=\"/unterkunft/trockentoiletten\">Trockentoiletten</a> sind ausserhalb der Häuser vorhanden.</p>\r\n"
            -
                title: PREISE
                content: "<div class=\"tarif\">\r\n    <dl>\r\n        <dt>2 - 10 Jahre</dt>\r\n        <dd>20<i>chf</i></dd>\r\n    </dl>\r\n    <dl>\r\n        <dt>10 - 16 Jahre</dt>\r\n        <dd>35<i>chf</i></dd>\r\n    </dl>\r\n    <dl>\r\n        <dt>Erwachsene</dt>\r\n        <dd>55<i>chf</i></dd>\r\n    </dl>\r\n    <p class=\"hint\">Gäste, die nicht im Restaurant essen, müssen einen Aufpreis von 5.- pro Nacht zahlen.</p>\r\n    <p class=\"hint\">Ermässigung ab der 2. Nacht.</p>\r\n</div>\r\n<p>Der Übernachtungspreis beinhaltet:</p>\r\n<ul>\r\n    <li>Eine Matratze und eine Decke *</li>\r\n    <li>Ein Frühstück</li>\r\n    <li>Holz nicht enthalten, kann vor Ort gekauft werden</li>\r\n    <li>Laternen, um Licht zu machen</li>\r\n    <li>Gemeinschaftliche Trockentoiletten draussen</li>\r\n    <li>Kostenlos für Kinder unter 2 Jahren</li>\r\n</ul>\r\n<p>* Wir empfehlen Ihnen, für Ihr Wohlbefinden, sich mit einem Schlafsack zu wappnen, zusätzlich zu den Decken, die Ihnen zur Verfügung stehen.</p>\r\n<p>Der erste Sack Holz ist gratis, jeder weitere kostet 9.- pro Sack.</p>"
            -
                title: FOTO-GALERIE
                gallery:
                    -
                        image: gite-cabane.jpg
                    -
                        image: gite-cabane_00.jpg
                    -
                        image: gite-cabane_01.jpg
                    -
                        image: gite-cabane_02.jpg
                    -
                        image: gite-cabane_03.jpg
                    -
                        image: gite-cabane_04.jpg
                    -
                        image: gite-cabane_05.jpg
                    -
                        image: gite-cabane_06.jpg
                    -
                        image: gite-cabane_07.jpg
    -
        id: gite-cabane-aside
        class: block
        contents:
            -
                title: 'ANDERE UNTERKÜNFTE'
                content: "<ul class=\"list\">\r\n    <li>\r\n        <article>\r\n            <figure markdown=\"1\">![Schlafsaal](gite-dortoir.jpg)</figure>\r\n            <h2 class=\"title\"><a href=\"/unterkunft/schlafsale-gasthaus\">Schlafsäle</a></h2>\r\n            <span class=\"category\">Gasthaus</span>\r\n            <p>Verbringen Sie eine Nacht alleine oder zu mehreren in einem unserer Schlafsäle im Gasthaus.</p>\r\n            <button class=\"more\"><span>Mehr erfahren</span></button>\r\n        </article>\r\n    </li>\r\n    <li>\r\n        <article>\r\n            <figure markdown=\"1\">![Tipi](gite-tipi.jpg)</figure>\r\n            <h2 class=\"title\"><a href=\"/unterkunft/indianer-tipis\">Indianer-Tipis</a></h2>\r\n            <span class=\"category\">Unterkunft</span>\r\n            <p>Erleben Sie eine Nacht in einem echten indianischen Tipi.</p>\r\n            <button class=\"more\"><span>Mehr erfahren</span></button>\r\n        </article>\r\n    </li>\r\n</ul>"
slug: hutten-wilder-western
---

Ein unvergesslicher Aufenthalt wie echte Cowboys