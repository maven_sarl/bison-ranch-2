---
title: INDIANER-TIPIS
media_order: 'gite-cabane_00.jpg,gite-cabane_01.jpg,gite-cabane_02.jpg,gite-cabane_03.jpg,gite-cabane_04.jpg,gite-cabane_05.jpg,gite-cabane_06.jpg,gite-cabane_07.jpg,gite-cabane.jpg,gite-dortoir.jpg,gite-tipi.jpg,wallpaper_gite_cabane.jpg'
meta_title: Indianer-Tipis
meta_description: 'Erleben Sie echte indianische Tipis'
background_image: wallpaper_gite_tipi.jpg
contents:
    -
        id: gite-cabane
        class: block
        aside:
            -
                content: "<div class=\"information\">\r\n        <p>Reservierungen sind möglich von Mittwoch bis Sonntag. Wir haben montags und dienstags geschlossen.</p>\r\n\r\n        <ul>\r\n            <li>Ankunft: <i>15h30</i></li>\r\n            <li>Abreise: <i>11h</i></li>\r\n            <li>Frühstück: <i>8h30 bis 10h30</i></li>\r\n        </ul>\r\n </div>\r\n<nav class=\"subnavigation\">\r\n    <ul>\r\n        <li markdown=\"1\">[Reservierung <i>Kontaktformular</i>](/contact)</li>\r\n        <li markdown=\"1\">[Anfahrtsplan <i>Wo Sie uns finden</i>](/contact#access)</li>\r\n    </ul>\r\n</nav>"
        contents:
            -
                title: 'UNSERE INDIANER-TIPIS'
                content: "<p class=\"preamble\">Für Indianerfans bietet die Bison Ranch zwei Tipis mit je 10 Plätzen, ein Tipi mit 3 Plätzen und zwei Tipis mit je 4 Plätzen an</p>\r\n<p>Jedes Tipi ist mit Liegen und einer zentralen Feuerstelle ausgestattet, um Sie warm zu halten. Die gemeinschaftlichen <a href=\"/unterkunft/trockentoiletten\">Trockentoiletten</a> sind draussen zugänglich.</p>\r\n"
            -
                title: PREISE
                content: "<div class=\"tarif\">\r\n<dl>\r\n    <dt>2 - 10 Jahre</dt>\r\n    <dd>20<i>chf</i></dd>\r\n</dl>\r\n<dl>\r\n    <dt>10 - 16 Jahre</dt>\r\n    <dd>25<i>chf</i></dd>\r\n</dl>\r\n<dl>\r\n    <dt>Erwachsene</dt>\r\n    <dd>40<i>chf</i></dd>\r\n</dl>\r\n<p class=\"hint\">Gäste, die nicht im Restaurant essen, müssen einen Aufpreis von 5.- pro Nacht zahlen.</p>\r\n</div>\r\n<p>Der Übernachtungspreis beinhaltet:</p>\r\n<ul>\r\n<li>Eine Liege und eine Decke *</li>\r\n<li>Ein Frühstück</li>\r\n<li>Holz nicht enthalten, kann vor Ort gekauft werden</li>\r\n<li>Gemeinschaftliche Trockentoiletten draussen</li>\r\n<li>Kostenlos für Kinder unter 2 Jahren</li>\r\n</ul>\r\n<p>* Wir empfehlen Ihnen, für Ihr Wohlbefinden, sich mit einem Schlafsack zu wappnen, zusätzlich zu den Decken, die Ihnen zur Verfügung stehen.</p>\r\n<p>Der erste Sack Holz ist gratis, jeder weitere kostet 9.- pro Sack</p>"
            -
                title: FOTO-GALERIE
                gallery:
                    -
                        image: gite-tipi.jpg
                    -
                        image: gite-tipi_00.jpg
                    -
                        image: gite-tipi_01.jpg
                    -
                        image: gite-tipi_02.jpg
                    -
                        image: gite-tipi_03.jpg
                    -
                        image: gite-tipi_04.jpg
    -
        id: gite-cabane-aside
        class: block
        contents:
            -
                title: 'ANDERE UNTERKÜNFTE'
                content: "<ul class=\"list\">\r\n    <li>\r\n        <article>\r\n            <figure markdown=\"1\">![Schlafsaal](gite-dortoir.jpg)</figure>\r\n            <h2 class=\"title\"><a href=\"/unterkunft/schlafsale-gasthaus\">Schlafsäle</a></h2>\r\n            <span class=\"category\">Gasthaus</span>\r\n            <p>Verbringen Sie eine Nacht alleine oder zu mehreren in einem unserer Schlafsäle im Gasthaus.</p>\r\n            <button class=\"more\"><span>Mehr erfahren</span></button>\r\n        </article>\r\n    </li>\r\n    <li>\r\n        <article>\r\n            <figure markdown=\"1\">![Hütte](gite-cabane.jpg)</figure>\r\n            <h2 class=\"title\"><a href=\"/unterkunft/hutten-wilder-western\">Hütten Wilder Westen</a></h2>\r\n            <span class=\"category\">Unterkunft</span>\r\n            <p>Wohnen Sie als Paar oder als Familie in einer unserer Hütten wie echte Cowboys.</p>\r\n            <button class=\"more\"><span>Mehr erfahren</span></button>\r\n        </article>\r\n    </li>\r\n</ul>"
slug: indianer-tipis
---

Erleben Sie echte indianische Tipis