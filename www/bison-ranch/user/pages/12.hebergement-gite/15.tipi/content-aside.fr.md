---
title: 'Tipis indiens'
media_order: 'wallpaper_gite_tipi.jpg,gite-cabane.jpg,gite-dortoir.jpg,gite-tipi_00.jpg,gite-tipi_01.jpg,gite-tipi_02.jpg,gite-tipi_03.jpg,gite-tipi_04.jpg,gite-tipi.jpg'
meta_title: 'Tipis Indiens'
meta_description: 'Vivez l''expérience d''un authentique tipi indien'
background_image: wallpaper_gite_tipi.jpg
contents:
    -
        id: gite-cabane
        class: block
        aside:
            -
                content: "<div class=\"information\">\r\n    <p>Les réservations sont possibles du mercredi au dimanche. Nous sommes fermés le lundi et mardi.</p>\r\n    <ul>\r\n        <li>Heure d'arrivée : <i>15h30</i></li>\r\n        <li>Heure de départ : <i>11h</i></li>\r\n        <li>Petit déjeuner : <i>8h30 à 10h30</i></li>\r\n    </ul>\r\n</div>\r\n<nav class=\"subnavigation\">\r\n    <ul>\r\n        <li markdown=\"1\">[Réservation <i>Formulaire de contact</i>](/contact)</li>\r\n        <li markdown=\"1\">[Plan d'accès <i>Ou nous trouver ?</i>](/contact#access)</li>\r\n    </ul>\r\n</nav>"
        contents:
            -
                title: 'NOS TIPIS INDIENS'
                content: "<p class=\"preamble\">Pour les amateurs d'indiens, le Bison Ranch vous propose deux tipis de 10 places, un tipi de 3 places et deux tipis de 4 places.</p>\r\n<p>Chaque tipi est muni de couchettes et d'un foyer central pour vous tenir chaud. Les <a href=\"/hebergement-gite/toilettes-seches\">toilettes sèches</a> communes sont disponibles à l'extérieur.</p>\r\n"
            -
                title: Tarifs
                content: "<div class=\"tarif\">\r\n<dl>\r\n<dt>2 à 10 ans</dt>\r\n<dd>20<i>chf</i></dd>\r\n</dl>\r\n<dl>\r\n<dt>10 - 16 ans</dt>\r\n<dd>25<i>chf</i></dd>\r\n</dl>\r\n<dl>\r\n<dt>Adulte</dt>\r\n<dd>40<i>chf</i></dd>\r\n</dl>\r\n<p class=\"hint\">Les personnes ne mangeant pas au restaurant devront s'acquitter d'un supplément de 5.-/par nuit.</p>\r\n</div>\r\n<p>Le prix de la nuitée inclut:</p>\r\n<ul>\r\n<li>Une couchette et une couverture *</li>\r\n<li>Un petit déjeuner</li>\r\n<li>Bois non fourni, peut être acheté sur place</li>\r\n<li>Toilettes sèches communes à l'extérieur</li>\r\n<li>Gratuit pour les enfants de moins de 2ans</li>\r\n</ul>\r\n<p>* Il est vivement conseillé, pour votre confort, de vous munir de votre sac de couchage, malgré nos couvertures à disposition.</p>\r\n<p>Le premier sac de bois est offert à partir de deux achetés (9.-/sac)</p>"
            -
                title: 'GALERIE DE PHOTOS'
                gallery:
                    -
                        image: gite-tipi.jpg
                    -
                        image: gite-tipi_00.jpg
                    -
                        image: gite-tipi_01.jpg
                    -
                        image: gite-tipi_02.jpg
                    -
                        image: gite-tipi_03.jpg
                    -
                        image: gite-tipi_04.jpg
    -
        id: gite-cabane-aside
        class: block
        contents:
            -
                title: 'AUTRES LOGEMENTS À DISPOSITION'
                content: "<ul class=\"list\">\r\n    <li>\r\n        <article>\r\n            <figure markdown=\"1\">![](gite-dortoir.jpg)</figure>\r\n            <h2 class=\"title\"><a href=\"/hebergement-gite/dortoir\">Dortoirs</a></h2>\r\n            <span class=\"category\">Hébergement</span>\r\n            <p>Passez une nuit seul ou à plusieurs dans l'un de nos dortoirs situés à l'auberge.</p>\r\n            <button class=\"more\"><span>En savoir plus</span></button>\r\n        </article>\r\n    </li>\r\n    <li>\r\n        <article>\r\n            <figure markdown=\"1\">![](gite-cabane.jpg)</figure>\r\n            <h2 class=\"title\"><a href=\"/hebergement-gite/cabane\">Cabanes Far West</a></h2>\r\n            <span class=\"category\">Gîte</span>\r\n            <p>Séjournez en couple ou en famille dans une l'une de nos cabanes comme de vrais cowboys.</p>\r\n            <button class=\"more\"><span>En savoir plus</span></button>\r\n        </article>\r\n    </li>\r\n</ul>"
---

Vivez l'expérience d'un authentique tipi indien