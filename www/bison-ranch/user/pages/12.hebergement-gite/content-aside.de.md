---
title: UNTERKUNFT
media_order: 'gite-cabane.jpg,gite-dortoir.jpg,gite-feu-de-camp.jpg,gite-tipi.jpg,wallpaper_gite.jpg'
meta_title: Unterkunft
meta_description: 'Hütten Wilder Westen - Tipis - Schlafsäle'
background_image: wallpaper_gite.jpg
contents:
    -
        id: gite-information
        class: block
        aside:
            -
                content: "<div class=\"information\">\r\n        <p>Reservierungen sind möglich von Mittwoch bis Sonntag. Wir haben montags und dienstags geschlossen.</p>\r\n\r\n        <ul>\r\n            <li>Ankunft: <i>15h30</i></li>\r\n            <li>Abreise: <i>11h</i></li>\r\n            <li>Frühstück: <i>8h30 bis 10h30</i></li>\r\n        </ul>\r\n </div>\r\n<nav class=\"subnavigation\">\r\n    <ul>\r\n        <li markdown=\"1\">[Reservierung <i>Kontaktformular</i>](/contact)</li>\r\n        <li markdown=\"1\">[Anfahrtsplan <i>Wo Sie uns finden</i>](/contact#access)</li>\r\n    </ul>\r\n</nav>"
        contents:
            -
                title: 'Eine Nacht wie im Wilden Westen!'
                content: "**Nach einem langen Tag voller Aktivitäten auf der Bison Ranch, warum nicht über Nacht bleiben und den Zauber und Western-Charme wirken lassen.**\r\n\r\nBeenden Sie Ihren Tag im Gasthaus zu Country-Musik oder zum Knistern des guten alten Lagerfeuers, vor dem verdienten Erholungsschlaf."
                image: gite-feu-de-camp.jpg
            -
                content: "Aber Vorsicht, auf der Bison Ranch zu übernachten ist nichts für Stubenhocker! Ade Badezimmer-Luxus, Toilettenspülung, Klimaanlage und elektrische Heizung.\r\n\r\nHerzlich Willkommen im Wilden Westen mit dem Allernötigsten: ein Dach, ein Feuer und Decken, um Sie zu wärmen."
    -
        id: gite
        class: block
        contents:
            -
                title: 'Unsere Unterkünfte'
                content: "<p>Bison Ranch bietet Ihnen verschiedene Unterkünfte für eine oder mehrere Nächte. Sie entscheiden, ob Sie lieber im Schlafsaal, in der Hütte aus dem Wilden Westen oder im Indianer-Tipi übernachten.</p>\r\n<p>Ein guter Moment der Entspannung für Gross und Klein, im Sommer wie im Winter!</p>\r\n<ul class=\"list\">\r\n    <li>\r\n        <article>\r\n            <figure markdown=\"1\">![Schlafsaal](gite-dortoir.jpg)</figure>\r\n            <h2 class=\"title\"><a href=\"/unterkunft/schlafsale-gasthaus\">Schlafsäle</a></h2>\r\n            <span class=\"category\">Gasthaus</span>\r\n            <p>Verbringen Sie eine Nacht alleine oder zu mehreren in einem unserer Schlafsäle im Gasthaus.</p>\r\n            <button class=\"more\"><span>Mehr erfahren</span></button>\r\n        </article>\r\n    </li>\r\n    <li>\r\n        <article>\r\n            <figure markdown=\"1\">![Hütte](gite-cabane.jpg)</figure>\r\n            <h2 class=\"title\"><a href=\"/unterkunft/hutten-wilder-western\">Hütten Wilder Westen</a></h2>\r\n            <span class=\"category\">Unterkunft</span>\r\n            <p>Wohnen Sie als Paar oder als Familie in einer unserer Hütten wie echte Cowboys.</p>\r\n            <button class=\"more\"><span>Mehr erfahren</span></button>\r\n        </article>\r\n    </li>\r\n    <li>\r\n        <article>\r\n            <figure markdown=\"1\">![Tipi](gite-tipi.jpg)</figure>\r\n            <h2 class=\"title\"><a href=\"/unterkunft/indianer-tipis\">Indianer-Tipis</a></h2>\r\n            <span class=\"category\">Unterkunft</span>\r\n            <p>Erleben Sie eine Nacht in einem echten indianischen Tipi.</p>\r\n            <button class=\"more\"><span>Mehr erfahren</span></button>\r\n        </article>\r\n    </li>\r\n</ul>"
slug: unterkunft
---

Hütten Wilder Westen - Tipis - Schlafsäle