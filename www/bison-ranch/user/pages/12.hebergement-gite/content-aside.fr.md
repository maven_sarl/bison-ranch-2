---
title: 'HÉBERGEMENT ET GÎTE'
media_order: 'gite-cabane.jpg,gite-dortoir.jpg,gite-feu-de-camp.jpg,gite-tipi.jpg,wallpaper_gite.jpg'
meta_title: 'Hébergement et gîte'
meta_description: 'Cabanes du Far West - Tipis - Dortoirs'
background_image: wallpaper_gite.jpg
contents:
    -
        id: gite-information
        class: block
        aside:
            -
                content: "<div class=\"information\">\r\n    <p>Les réservations sont possibles du mercredi au dimanche. Nous sommes fermés le lundi et mardi.</p>\r\n    <ul>\r\n        <li>Heure d'arrivée : <i>15h30</i></li>\r\n        <li>Heure de départ : <i>11h</i></li>\r\n        <li>Petit déjeuner : <i>8h30 à 10h30</i></li>\r\n    </ul>\r\n</div>\r\n<nav class=\"subnavigation\">\r\n    <ul>\r\n        <li markdown=\"1\">[Réservation <i>Formulaire de contact</i>](/contact)</li>\r\n        <li markdown=\"1\">[Plan d'accès <i>Ou nous trouver ?</i>](/contact#access)</li>\r\n    </ul>\r\n</nav>"
        contents:
            -
                title: 'UNE NUIT COMME AU FAR WEST !'
                content: "**Après une longue journée d'activités au Bison Ranch, pourquoi ne pas y rester une nuit afin que la magie et le charme Western continue d'opérer.**\r\n\r\nTerminez votre journée au son de la musique country à l'auberge ou encore au crépitement d'un bon vieux feu de camp avant un sommeil bien mérité."
                image: gite-feu-de-camp.jpg
            -
                content: "Mais attention, dormir au Bison Ranch, ce n'est pas fait pour les pantouflards ! Adieu le luxe d'une salle de bain, toilettes à chasse d'eau, climatisation ou encore chauffage éléctrique.\r\n\r\nVous voilà revenu à l'époque du Far West avec le strict minimum: un toit, du feu et des couvertures pour vous chauffer."
    -
        id: gite
        class: block
        contents:
            -
                title: 'Les logements proposés'
                content: "<p>Bison Ranch vous propose plusieurs types de logements pour une ou plusieurs nuits. A vous de voir si vous êtes plutôt: dortoirs, cabanes Far West ou encore tipis indiens.</p>\r\n<p>Un bon moment de détente pour petits et grands, en été, comme en hiver !</p>\r\n<ul class=\"list\">\r\n    <li>\r\n        <article>\r\n            <figure markdown=\"1\">![](gite-dortoir.jpg)</figure>\r\n            <h2 class=\"title\"><a href=\"/hebergement-gite/dortoir\">Dortoirs</a></h2>\r\n            <span class=\"category\">Hébergement</span>\r\n            <p>Passez une nuit seul ou à plusieurs dans l'un de nos dortoirs situés à l'auberge.</p>\r\n            <button class=\"more\"><span>En savoir plus</span></button>\r\n        </article>\r\n    </li>\r\n    <li>\r\n        <article>\r\n            <figure markdown=\"1\">![](gite-cabane.jpg)</figure>\r\n            <h2 class=\"title\"><a href=\"/hebergement-gite/cabane\">Cabanes Far West</a></h2>\r\n            <span class=\"category\">Gîte</span>\r\n            <p>Séjournez en couple ou en famille dans une l'une de nos cabanes comme de vrais cowboys.</p>\r\n            <button class=\"more\"><span>En savoir plus</span></button>\r\n        </article>\r\n    </li>\r\n    <li>\r\n        <article>\r\n            <figure markdown=\"1\">![](gite-tipi.jpg)</figure>\r\n            <h2 class=\"title\"><a href=\"/hebergement-gite/tipi\">Tipis indiens</a></h2>\r\n            <span class=\"category\">Gîte</span>\r\n            <span>de mi-mai à mi-octobre</span>\r\n            <p>Vivez l'expérience d'une nuit dans un authentique tipi indien.</p>\r\n            <button class=\"more\"><span>En savoir plus</span></button>\r\n        </article>\r\n    </li>\r\n</ul>"
---

Cabanes du Far West - Tipis - Dortoirs 