---
title: 'Main Category'
child_type: category
content:
    items: '@self.children'
    order:
        by: date
        dir: asc
---

