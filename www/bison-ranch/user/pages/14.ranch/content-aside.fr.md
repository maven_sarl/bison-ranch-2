---
title: 'A Propos du Ranch'
meta_title: 'Notre Ranch'
meta_description: 'Domaine des Colisses du Bas'
background_image: wallpaper_ranch.jpg
contents:
    -
        id: ranch
        class: block
        aside:
            -
                widget: address
        contents:
            -
                title: 'LES COLISSES DU BAS'
                image: colisse-du-bas.jpg
            -
                content: "Bison Ranch est situé au Les Colisses du Bas à 1196m d'alitude sur le versant sud-est du Chasseral, dans le district de la Neuveville et la commune de Nods, il est relié côté ouest à sa commune par une piste de 5 km, et côté est aux Prés-d'Orvin par une route carrossable de 3,5 km.\r\n\r\n<a class=\"more\" href=\"https://www.google.ch/maps/place/Bison+Ranch/@47.1137309,7.4882166,5066a,20y,270h,81.27t/data=!3m1!1e3!4m2!3m1!1s0x478e04a129e9b2c5:0xc83c306e29d85eb4?hl=fr\" target=\"_blank\"><span>Voir sur Google Map</span></a>"
            -
                title: 'Histoire du ranch'
                image: ranch-photo.jpg
            -
                content: "<p class=\"preamble\">Pendant une vingtaine d’années, exploité par notre famille comme domaine d’estivage, <strong>le domaine des Colisses du Bas</strong> accueillait des vaches laitières et des génisses en estivage. Ce domaine n’était exploité que pendant les mois d’été.</p>\r\n<p>Le monde agricole étant en difficulté, nous prenons alors la décision de diversifier notre production et c’est alors que les 5 premiers bisons arrivent en 1992. Conjointement, nous ouvrons l’auberge la même année. <a href=\"/bison\">Notre troupeau de bisons</a> s’agrandit d’année en année par quelques achats en Belgique et à Genève, ainsi que 10 à 15 naissances par an et comporte actuellement une cinquantaine de bêtes.</p>\r\n<p>En 1997, nous vendons nos vaches laitières et axons de plus en plus notre travail sur le tourisme, les bisons et les chevaux.</p>\r\n<p>Des parcs d’estivage ont été conçus de telle sorte qu'environ 20 élèves étalons investissent les lieux pour l’été.</p>\r\n<p>Les génisses ont été remplacées par des chevaux de toutes races, de tout âge.</p>\r\n<h3><span>Nos premiers tipis</span></h3>"
                image: ranch-tipi.jpg
            -
                content: "<p>Les <a href=\"/hebergement-gite/tipi\">premiers tipis</a> ont été construits en 1993 et disparus en 2004.</p>\r\n<p>Depuis 2014, nous avons réinstallé <a href=\"/hebergement-gite/tipi\">nos tipis</a> et de <a href=\"/hebergement-gite/cabane\">nouvelles bâtisses en bois</a> dignes du Far West !</p>\r\n"
    -
        id: ranch-activites
        class: block
        contents:
            -
                title: Activités
                content: "<h3>Randonnée</h3>\r\n<p>A pieds, à cheval ou en vélo, vous pouvez parcourir des km et des km de chemins en pleine nature. En ¼ heure, vous serez sur la crête du Chasseral afin de découvrir par exemple de très bonne heure de superbes levers de soleil, accompagné du chant des oiseaux.</p>\r\n<p>Des VTT peuvent être loués et des tours organisés sur réservation. Renseignements au ranch.</p>\r\n<p>En hiver, 50 km de pistes de ski de fond vous attendent que ce soit à partir de Nods par les Prés-vaillons ou depuis les Prés-d’Orvin sur le Chasseral.</p>\r\n<p>Des sorties en raquettes sont aussi régulièrement organisées. <a href=\"/contact\">Renseignements au ranch</a>.</p>"
---

Domaine des Colisses du Bas