---
title: 'ÜBER DIE RANCH'
meta_title: 'Unsere Ranch'
meta_description: 'Das Gebiet Colisses du Bas'
background_image: wallpaper_ranch.jpg
contents:
    -
        id: ranch
        class: block
        aside:
            -
                widget: address
        contents:
            -
                title: 'LES COLISSES DU BAS'
                image: colisse-du-bas.jpg
            -
                content: "<p>Die Bison Ranch liegt in <strong>Colisses du Bas</strong> in 1196m Höhe auf dem südöstlichen Hang des <strong>Chasseral</strong>, im Bezirk von Neuveville und der Gemeinde Nods. Auf der Westseite ist sie durch einen 5km langen Weg mit Nods, auf der Ostseite durch eine befahrbare Strasse von 3,5km mit <strong>Prés-d'Orvin</strong> verbunden.</p>\r\n<a class=\"more\" href=\"https://www.google.ch/maps/place/Bison+Ranch/@47.1137309,7.4882166,5066a,20y,270h,81.27t/data=!3m1!1e3!4m2!3m1!1s0x478e04a129e9b2c5:0xc83c306e29d85eb4?hl=fr\" target=\"_blank\"><span>Auf Google Maps sehen</span></a>"
            -
                title: 'Geschichte der Ranch'
                image: ranch-photo.jpg
            -
                content: "<p class=\"preamble\">Während über 20 Jahren wurde <strong>das Gebiet Colisses du Bas</strong> von unserer Familie zur Viehwirtschaft genutzt, um Milchkühe und Färsen zu weiden. Diese Gegend wurde nur in den Sommermonaten genutzt.</p>\r\n<p>Als die Landwirtschaft schwierig wurde, entschieden wir uns die Produktion zu diversifizieren und so kamen im Jahr 1992 die ersten 5 Bisons zu uns. Im gleichen Jahr eröffneten wir das Gasthaus. <a href=\"/bison\">Unsere Bisonherde</a> wuchs von Jahr zu Jahr durch ein paar Zukäufe aus Belgien und Genf sowie durch etwa 10-15 Geburten und umfasst derzeit etwa fünfzig Tiere.</p>\r\n<p>1997 verkauften wir unsere Milchkühe und richteten unsere Arbeit immer mehr auf den Tourismus, die Bisons und die Pferde aus.</p>\r\n<p>Die Weiden wurden so ausgelegt, dass ca. 20 junge Zuchthengste den Ort im Sommer nutzen können.</p>\r\n<p>Die Färsen wurden durch Pferde aller Rassen und allen Alters ersetzt.</p>\r\n<h3><span>Unsere ersten Tipis</span></h3>"
                image: ranch-tipi.jpg
            -
                content: "<p>Die <a href=\"/unterkunft/indianer-tipis\">ersten Tipis</a> wurden 1993 gebaut und verschwanden 2004.</p>\r\n<p>Seit 2014 haben wir <a href=\"/unterkunft/indianer-tipis\">unsere Tipis</a> wieder aufgebaut, sowie <a href=\"/unterkunft/hutten-wilder-western\">neue Holzhütten</a> aus dem Wilden Westen!</p>\r\n"
    -
        id: ranch-activites
        class: block
        contents:
            -
                title: Activités
                content: "<h3>Wanderung</h3>\r\n<p>Ob zu Fuss, zu Pferd oder mit dem Velo, Sie können kilometerlange Wege mitten in der Natur zurücklegen. In 1/4 Stunde sind Sie auf dem Chasseral-Kamm und können zum Beispiel zur frühen Stunde wunderschöne Sonnenaufgänge geniessen, begleitet von Vogelgesang.</p>\r\n<p>Mountainbikes können geliehen und Touren auf Reservierung organisiert werden. Infos auf der Ranch.</p>\r\n<p>Im Winter warten 50km Langlaufpiste auf Sie, von Nods aus über Présvaillons oder von Près-d'Orvin aus über den Chasseral.</p>\r\n<p>Auch Schneeschuhwanderungen werden regelmässig organisiert. <a href=\"http://bisonranch.ch/de/kontakt\">Infos auf der Ranch.</a>.</p>"
---

Das Gebiet Colisses du Bas