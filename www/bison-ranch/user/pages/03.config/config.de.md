---
googletag_head: "<!-- Google Tag Manager -->\r\n<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':\r\nnew Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],\r\nj=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=\r\n'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);\r\n})(window,document,'script','dataLayer','GTM-M4LTCM');</script>\r\n<!-- End Google Tag Manager —>"
googletag_body: "<!-- Google Tag Manager (noscript) -->\r\n<noscript><iframe src=\"https://www.googletagmanager.com/ns.html?id=GTM-M4LTCM\"\r\nheight=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe></noscript>\r\n<!-- End Google Tag Manager (noscript) —>"
facebook: 'https://fr-fr.facebook.com/bisonranch.ch/'
holidays:
    -
        label: Winterpause
        start_date: '16-12-2019 00:00'
        end_date: '09-01-2020 23:59'
navigation_main:
    -
        name: Home
        internal_url: /home
    -
        name: News
        internal_url: /actualites
    -
        name: Gasthaus
        internal_url: /auberge-restauration
    -
        name: Unterkunft
        internal_url: /hebergement-gite
    -
        name: Bison
        internal_url: /bison
    -
        name: Ranch
        internal_url: /ranch
    -
        name: Medien
        internal_url: /medias
    -
        name: Kontakt
        internal_url: /contact-reservation
footer_col:
    -
        title: 'Bison Ranch'
        navigation:
            -
                name: Unterkunft
                internal_url: /hebergement-gite
            -
                name: 'Gasthaus und verpflegung'
                internal_url: /auberge-restauration
            -
                name: Bisons
                internal_url: /bison
            -
                name: 'Die ranch'
                internal_url: /ranch
    -
        title: 'Andere Aktivitäten'
        navigation:
            -
                name: 'Forest Jump'
                external_url: 'http://www.forestjump.ch/index.php/fr/'
            -
                name: Reiterhof
                external_url: 'http://www.centre-equestre-diesse.ch/'
            -
                name: 'Gleitschirmfliegen Chasseral'
                external_url: 'http://www.zorro.ch/fr/'
            -
                name: 'Reitanlage in Saignelégier'
                external_url: 'http://manegefm.ch/'
            -
                name: 'Velo-Verleih - Orvin'
                external_url: 'http://labici.ch/site/'
            -
                name: 'Velo-Verleih - Tramelan'
                external_url: 'https://fabien-bike.ch'
            -
                name: 'Pferde-Verleih - Chaumont'
                external_url: 'http://www.topekaranch.ch/articles.php?lng=fr&pg=12'
            -
                name: 'Pferde-Verleih - Chaux-des-Breuleux'
                external_url: 'http://ecuriedoublec.ch/Site/home_fr.html'
    -
        title: Info
        navigation:
            -
                name: News
                internal_url: /actualites
            -
                name: Öffnungszeiten
                internal_url: /contact-reservation
            -
                name: Zugang
                internal_url: /contact-reservation
            -
                name: Kontakt
                internal_url: /contact-reservation
            -
                name: Trockentoiletten
                internal_url: /hebergement-gite/les-toilettes-seches
title: Config
---

