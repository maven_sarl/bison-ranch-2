---
googletag_head: "<!-- Google Tag Manager -->\r\n<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':\r\nnew Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],\r\nj=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=\r\n'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);\r\n})(window,document,'script','dataLayer','GTM-M4LTCM');</script>\r\n<!-- End Google Tag Manager —>"
googletag_body: "<!-- Google Tag Manager (noscript) -->\r\n<noscript><iframe src=\"https://www.googletagmanager.com/ns.html?id=GTM-M4LTCM\"\r\nheight=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe></noscript>\r\n<!-- End Google Tag Manager (noscript) —>"
facebook: 'https://fr-fr.facebook.com/bisonranch.ch/'
holidays:
    -
        label: 'Fermeture hivernale'
        start_date: '16-12-2019 00:00'
        end_date: '09-01-2020 23:59'
navigation_main:
    -
        name: Home
        internal_url: /home
    -
        name: Actualités
        internal_url: /actualites
    -
        name: Auberge
        internal_url: /auberge-restauration
    -
        name: Gîte
        internal_url: /hebergement-gite
    -
        name: Bison
        internal_url: /bison
    -
        name: Ranch
        internal_url: /ranch
    -
        name: Médias
        internal_url: /medias
    -
        name: Contact
        internal_url: /contact-reservation
footer_col:
    -
        title: 'Bison Ranch'
        navigation:
            -
                name: 'hébergement et gîte'
                internal_url: /hebergement-gite
            -
                name: 'Auberge et restauration'
                internal_url: /auberge-restauration
            -
                name: Bisons
                internal_url: /bison
            -
                name: 'Le Ranch'
                internal_url: /ranch
    -
        title: 'Autres activités'
        navigation:
            -
                name: 'Forest Jump'
                external_url: 'http://www.forestjump.ch/index.php/fr/'
            -
                name: 'Centre équestre'
                external_url: 'http://www.centre-equestre-diesse.ch/'
            -
                name: 'Parapente Chasseral'
                external_url: 'http://www.zorro.ch/fr/'
            -
                name: 'Centre équestre à Saignelégier'
                external_url: 'http://manegefm.ch/'
            -
                name: 'Location vélo - Orvin'
                external_url: 'http://labici.ch/site/'
            -
                name: 'Location de vélo à Tramelan'
                external_url: 'https://fabien-bike.ch'
            -
                name: 'Location chevaux - Chaumont'
                external_url: 'http://www.topekaranch.ch/articles.php?lng=fr&pg=12'
            -
                name: 'Location chevaux - Chaux-des-Breuleux'
                external_url: 'http://ecuriedoublec.ch/Site/home_fr.html'
    -
        title: Info
        navigation:
            -
                name: Actualités
                internal_url: /actualites
            -
                name: Horaires
                internal_url: /contact-reservation
            -
                name: Accès
                internal_url: /contact-reservation
            -
                name: Contact
                internal_url: /contact-reservation
            -
                name: 'Toilettes sèches'
                internal_url: /hebergement-gite/les-toilettes-seches
title: Config
---

