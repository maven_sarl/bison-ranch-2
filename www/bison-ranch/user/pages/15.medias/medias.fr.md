---
title: Médias
media_order: 'Affiche_A4_animaux_dangeureux.pdf,affiche_bison_city.jpg,affiche_saint-bison_2015.pdf,affiche_saint-bison_2016.pdf,affiche_saint-bison_2017.pdf,Affiche_St_Bison_2011.pdf,Affiche_St_Bison_2012.pdf,Affiche_St_Bison_2013.pdf,agri-2017-25-p32.pdf,Animaux_dangeureux.pdf,Annonce_61x45.pdf,Annonce_85x61_1.pdf,Annonce_85x61_2.pdf,Annonce_85x61_nb_1.pdf,Annonce_85x61_nb_2.pdf,Annonce_122x45.pdf,Annonce_122x90.pdf,AnnonceA5_les_vendredi_BisonRanch.pdf,BisonRanch_DVPA_2012.pdf,BisonRanch_noir.pdf,Flyer_St_Bison_2011.pdf,Flyer_St_Bison_2012.pdf,Flyer_St_Bison_2013.pdf,Internet_Bison_Ranch.pdf,logo_couleur.pdf,Logo_gris.pdf,Logo_noir.pdf,Menu_Bison-Ranch.pdf,bille_en_tete_20130513.mp3,bille_en_tete_20130514.mp3,radio-fr.mp3,auberge01.jpg,auberge03.jpg,auberge04.jpg,auberge06.jpg,auberge07.jpg,auberge08.jpg,auberge09.jpg,auberge10.jpg,auberge11.jpg,auberge12.jpg,auberge13.jpg,city01.jpg,city02.jpg,city03.jpg,city04.jpg,city05.jpg,city06.jpg,city07.jpg,city08.jpg,city09.jpg,city10.jpg,city11.jpg,city12.jpg,bison01.jpg,bison02.jpg,bison03.jpg,bison04.jpg,bison05.jpg,bison06.jpg,bison07.jpg,bison08.jpg,bison09.jpg,bison10.jpg,bison11.jpg,bison12.jpg,bison13.jpg,bison14.jpg,bison15.jpg,bison16.jpg,situation01.jpg,situation02.jpg,situation03.jpg,situation04.jpg,situation05.jpg,situation06.jpg,situation07.jpg,situation08.jpg,situation09.jpg,situation10.jpg,situation11.jpg,situation12.jpg,situation13.jpg,situation14.jpg,situation15.jpg,situation16.jpg,situation17.jpg,situation18.jpg,situation19.jpg,situation20.jpg,wallpaper_homepage.jpg,bille-en-tete.png,bille-en-tete2.png,radio-fr-celine.png,video_amateur-youtube.png,video_officielle-youtube.png,video_reportage-forest-jump.png,video_reportage-grand-nord.png,affiche_saint-bison_2019.jpg,toilette-seche-explication.png,toilette-seche-explication.jpg,QR_code_Bison_facebook.png,QR_code_Bison_website.png,documentaire_Anders_als_die_Väter.png,emission_3sat_20.05.2019.jpg,affiche_saint-bison_2018.jpg,preview_BisonRanch_noir.png,preview_logo_couleur.png,preview_affiche_saint-bison_2018.png,preview_affiche_saint-bison_2019.png,preview_Animaux_dangeureux.png,preview_QR_code_Bison_facebook.png,preview_QR_code_Bison_website.png,preview_toilette-seche-explication.png'
meta_title: 'Medias et presse'
meta_description: 'Photos et téléchargements pour la presse'
background_image: wallpaper_homepage.jpg
media_contents:
    -
        title: 'Radio Fribourg - Bon plan du weekend'
        sub_title: 'mars 2016'
        filemedia: radio-fr.mp3
        image: radio-fr-celine.png
    -
        title: Vimeo
        sub_title: 'L''appel du grand nord'
        link: 'https://vimeo.com/79918390'
        image: video_reportage-grand-nord.png
    -
        title: 'Bille en tête RSR'
        sub_title: '13 mai 2013'
        filemedia: bille_en_tete_20130513.mp3
        image: bille-en-tete.png
    -
        title: 'Bille en tête RSR'
        sub_title: '14 mai 2013'
        filemedia: bille_en_tete_20130514.mp3
        image: bille-en-tete2.png
    -
        title: Interview
        sub_title: 'Canal Alpha'
        link: 'http://www.canalalpha.ch/emissions/autres-emissions/ya10ans/y-a-10-ans-y-a-10-ans-christian-lecomte-senvoyait-en-lair-dans-les-arbres/'
        image: video_reportage-forest-jump.png
    -
        title: Youtube
        sub_title: 'Vidéo Officielle Bison Ranch'
        link: 'https://www.youtube.com/watch?v=dTXo1lv6jyU'
        image: video_officielle-youtube.png
    -
        title: 'Anders als die Väter mit franz -  3SAT'
        sub_title: '20 mai 2019'
        link: 'https://youtu.be/LeUzse79pUM'
        image: emission_3sat_20.05.2019.jpg
media_gallery:
    -
        image: auberge01.jpg
    -
        image: auberge03.jpg
    -
        image: auberge04.jpg
    -
        image: auberge06.jpg
    -
        image: auberge07.jpg
    -
        image: auberge08.jpg
    -
        image: auberge09.jpg
    -
        image: auberge10.jpg
    -
        image: auberge11.jpg
    -
        image: auberge12.jpg
    -
        image: auberge13.jpg
    -
        image: city01.jpg
    -
        image: city02.jpg
    -
        image: city03.jpg
    -
        image: city04.jpg
    -
        image: city05.jpg
    -
        image: city06.jpg
    -
        image: city07.jpg
    -
        image: city08.jpg
    -
        image: city09.jpg
    -
        image: city10.jpg
    -
        image: city11.jpg
    -
        image: city12.jpg
    -
        image: bison01.jpg
    -
        image: bison02.jpg
    -
        image: bison03.jpg
    -
        image: bison04.jpg
    -
        image: bison05.jpg
    -
        image: bison06.jpg
    -
        image: bison07.jpg
    -
        image: bison08.jpg
    -
        image: bison09.jpg
    -
        image: bison10.jpg
    -
        image: bison11.jpg
    -
        image: bison12.jpg
    -
        image: bison13.jpg
    -
        image: bison14.jpg
    -
        image: bison15.jpg
    -
        image: bison16.jpg
    -
        image: situation01.jpg
    -
        image: situation02.jpg
    -
        image: situation03.jpg
    -
        image: situation04.jpg
    -
        image: situation05.jpg
    -
        image: situation06.jpg
    -
        image: situation07.jpg
    -
        image: situation08.jpg
    -
        image: situation09.jpg
    -
        image: situation10.jpg
    -
        image: situation11.jpg
    -
        image: situation12.jpg
    -
        image: situation13.jpg
    -
        image: situation14.jpg
    -
        image: situation15.jpg
    -
        image: situation16.jpg
    -
        image: situation17.jpg
    -
        image: situation18.jpg
    -
        image: situation19.jpg
    -
        image: situation20.jpg
media_ressources:
    -
        label: 'Texte Bison Ranch affiche '
        image_preview: preview_BisonRanch_noir.png
        link: '[noir](BisonRanch_noir.pdf?target=_blank)'
    -
        label: 'Logo Bison Ranch'
        image_preview: preview_logo_couleur.png
        link: '[couleur](logo_couleur.pdf?target=_blank) | [noir](Logo_noir.pdf?target=_blank) | [niveau gris](Logo_gris.pdf?target=_blank)'
    -
        label: 'Affiche animaux dangeureux'
        image_preview: preview_Animaux_dangeureux.png
        link: '[105x297mm](Animaux_dangeureux.pdf?target=_blank) | [A4](Affiche_A4_animaux_dangeureux.pdf?target=_blank)'
    -
        label: 'Affiche 28e Saint Bison 2019'
        image_preview: preview_affiche_saint-bison_2019.png
        link: '[A4](affiche_saint-bison_2019.jpg?target=_blank)'
    -
        label: 'Affiche 27e Saint Bison 2018'
        image_preview: preview_affiche_saint-bison_2018.png
        link: '[A4](affiche_saint-bison_2018.jpg?target=_blank)'
    -
        label: 'Toilettes sèches'
        image_preview: preview_toilette-seche-explication.png
        link: '[Dessin](toilette-seche-explication.jpg?target=_blank)'
    -
        label: 'QR Code Facebook'
        image_preview: preview_QR_code_Bison_facebook.png
        link: '[image PNG](QR_code_Bison_facebook.png?target=_blank)'
    -
        label: 'QR Code Site internet'
        image_preview: preview_QR_code_Bison_website.png
        link: '[image PNG](QR_code_Bison_website.png?target=_blank)'
    -
        label: Archives
        link: "**Bison Ranch News**<br/>\r\n[A4](Internet_Bison_Ranch.pdf?target=_blank)\r\n\r\n**Annonces noir et blanc**<br/>\r\n[61x45mm](Annonce_61x45.pdf?target=_blank) | [122x45mm](Annonce_122x45.pdf?target=_blank) | [122x90mm](Annonce_122x90.pdf?target=_blank)\r\n\r\n**Affiche 20 ans  Bison Ranch**<br/>\r\n[A4](affiche_bison_city.jpg?target=_blank)\r\n\r\n**Affiche Saint-Bison 2017**<br/>\r\n[A4](Affiche_St_Bison_2017.pdf?target=_blank) | [Flyer](Flyer_St_Bison_2017.pdf?target=_blank)\r\n\r\n**Affiche Saint-Bison 2016**<br/>\r\n[A4](Affiche_St_Bison_2016.pdf?target=_blank) | [Flyer](Flyer_St_Bison_2016.pdf?target=_blank)\r\n\r\n**Affiche Saint-Bison 2015**<br/>\r\n[A4](Affiche_St_Bison_2015.pdf?target=_blank) | [Flyer](Flyer_St_Bison_2015.pdf?target=_blank)\r\n\r\n**Affiche Saint-Bison 2013**<br/>\r\n[A4](Affiche_St_Bison_2013.pdf?target=_blank) | [Flyer](Flyer_St_Bison_2013.pdf?target=_blank)\r\n\r\n**Affiche Saint-Bison 2012**<br/>\r\n[A4](Affiche_St_Bison_2012.pdf?target=_blank) | [Flyer](Flyer_St_Bison_2012.pdf?target=_blank)\r\n\r\n**Affiche Saint-Bison 2011**<br/>\r\n[A4](Affiche_St_Bison_2011.pdf?target=_blank) | [Flyer](Flyer_St_Bison_2011.pdf?target=_blank)\r\n\r\n**Bison Ranch - Agrotouristique**<br/>\r\n[Le dossier](BisonRanch_DVPA_2012.pdf?target=_blank)"
---

Photos et téléchargements