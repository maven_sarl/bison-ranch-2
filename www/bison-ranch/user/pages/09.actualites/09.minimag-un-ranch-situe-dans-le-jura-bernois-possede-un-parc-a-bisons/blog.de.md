---
title: 'MINIMAG: UN RANCH SITUÉ DANS LE JURA BERNOIS POSSÈDE UN PARC À BISONS'
media_order: bison-blog.jpeg
background_image: bison-blog.jpeg
excerpt: 'Ce lieu offre un dépaysement certains tout en proposant des activités comme le tir à l''arc ou des nuits sous tipis.'
gallery:
    -
        image: bison-blog.jpeg
    -
        image: bison-blog.jpeg
    -
        image: bison-blog.jpeg
date: '23-02-2017 09:14'
taxonomy:
    tag:
        - bison
    category:
        - la-vie-au-ranch
        - divers
body_classes: page-news
category: divers
---

Ce lieu offre un dépaysement certains tout en proposant des activités comme le tir à l'arc ou des nuits sous tipis.

[http://www.rts.ch/play/tv/12h45/video/minimag-un-r...](http://www.rts.ch/play/tv/12h45/video/minimag-un-ranch-situe-dans-le-jura-bernois-possede-un-parc-a-bisons?id=7823883&target=_blank)