---
title: '25E SAINT BISON'
media_order: 'affiche_saint-bison_2016.pdf,st-bison-2016.png'
background_image: st-bison-2016.png
excerpt: 'Le week-end du 10 au 12 juin 2016, nous fêtons la 25e édition de la Saint Bison !'
taxonomy:
    category:
        - la-vie-au-ranch
category: la-vie-au-ranch
---

Venez fêter avec nous au Bison Ranch - ambiance country tout le week-end :-)

Au programme :

Vendredi 10 juin

* dès 20h30 - Concert "Paul Mac Bonvin"

Samedi 11 juin

* 10h00 - Randonnée à cheval dans le Chasseral
* 20h30 - Concert "The Wild Orchids"
* 22h30 - Concert "Rober Thos and TCM Band"

Dimanche 12 juin

* Toute la journée - Maquillages pour les enfants
* dès 11h00 - Concert avec piano mécanique de 1892 et son comique
* dès 11h30 - Concerts "The Wild Orchids" et "Rober Thos and TCM Band"

[Voir l'affiche de la 25e Saint Bison](affiche_saint-bison_2016.pdf?target=_blank)