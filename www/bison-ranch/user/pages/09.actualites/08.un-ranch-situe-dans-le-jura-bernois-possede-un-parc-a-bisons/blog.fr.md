---
title: 'UN RANCH SITUÉ DANS LE JURA BERNOIS POSSÈDE UN PARC À BISONS'
media_order: 'Capture d''ecran 2016-06-27 a 22.40.29.png'
background_image: 'Capture d''ecran 2016-06-27 a 22.40.29.png'
excerpt: 'Reportage du 22 juin sur RTSinfo'
taxonomy:
    tag:
        - bison
    category:
        - la-vie-au-ranch
category: la-vie-au-ranch
---

<iframe src="//tp.srgssr.ch/p/rts/embed?urn=urn:rts:video:7823883&amp;start=" width="624" height="351" frameborder="0" name="Minimag: un ranch situé dans le Jura bernois possède un parc à bisons"></iframe>