---
title: 'VENDREDI, C''EST PERMIS !'
media_order: 'AnnonceA5_les_vendredi_BisonRanch.jpg,AnnonceA5_les_vendredi_BisonRanch.pdf'
background_image: AnnonceA5_les_vendredi_BisonRanch.jpg
excerpt: 'Chaque vendredi au Bison Ranch, tout est permis ! Découvrez nos prochaines dates de concerts et venez faire la fête avec nous !'
taxonomy:
    category:
        - la-vie-au-ranch
category: la-vie-au-ranch
---

Chaque vendredi au Bison Ranch, tout est permis !
Découvrez nos prochaines dates de concerts et venez faire la fête avec nous:

* Le vendredi 31 juillet 2015 
<br/>**Rober Thos and TCM Band**
* Le vendredi 7 aout 2015 
<br/>**Rod Barthet**
* Le vendredi 14 aout 2015 
<br/>**VBR**

Plus d'infos: [Flyer de l'événement ](AnnonceA5_les_vendredi_BisonRanch.pdf?target=_blank) ou [contactez-nous](/contact-reservation)