---
title: 'LA LANGUE DE BISON EST DE RETOUR !'
media_order: langue-bison.JPG
background_image: langue-bison.JPG
excerpt: 'Le Week-end du 21 au 22 novembre, venez nombreux déguster notre menu langue de bison.'
taxonomy:
    tag:
        - bison
    category:
        - la-vie-au-ranch
category: la-vie-au-ranch
---

Le Week-end du 21 au 22 novembre, découvrez notre menu langue de bison:

Soupe aux légumes

**

Langue de bison, sauce aux câpres
avec purée de p.d.t. maison et légumes de saison

**

Cornet à la crème

Pour CHF 29.50

*&nbsp;photo non contractuelle