---
title: 'SAINT BISON 28ÈME'
media_order: PHOTO-2019-05-06-09-45-53.jpg
background_image: PHOTO-2019-05-06-09-45-53.jpg
excerpt: 'Le 7, 8 et 9 juin 2019, venez fêter avec nous la 28ème de la Saint-Bison. Cantine chauffée, restauration chaude, bar et ambiance tout le week-end !'
---

Le 7, 8 et 9 juin 2019, venez fêter avec nous la 28ème de la Saint-Bison. 
Cantine chauffée, restauration chaude, bar et ambiance tout le week-end !

Au programme: 

Vendredi 7 juin: dès 21:00: Concert de Paul Mac Bonvin

Samedi 8 juin: dès 10:00: Randonnée à cheval sur le Chasseral, 12:00 concert avec piano mécanique et son comique, 18:00 Nivrozic ORVIN et 21:30 Tiffany et Christophe Meyer

Dimanche 9 juin: dès 11:30 Suisse mélodie et cor des Alpes et à 14:30 JO Mettraux (solo)

[Voir l'affiche de la Saint-Bison](https://bisonranch.ch/user/pages/09.actualites/12.saint-bison-28eme/PHOTO-2019-05-06-09-45-53.jpg?target=_blank)