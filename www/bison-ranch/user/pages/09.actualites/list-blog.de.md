---
title: News
media_order: wallpaper_gite.jpg
meta_title: News
background_image: wallpaper_gite.jpg
slug: news
routes:
    aliases:
        - /actu/la-vie-au-ranch
        - /actu/divers
content:
    items: '@self.children'
    order:
        by: folder
        dir: DESC
---

