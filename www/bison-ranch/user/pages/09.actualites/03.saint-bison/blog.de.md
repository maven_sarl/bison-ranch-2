---
title: SAINT-BISON
media_order: 'affiche_saint-bison_2015.pdf,saint-bison.jpg'
background_image: saint-bison.jpg
excerpt: 'Venez fêter avec nous la 24ème de la Saint-Bison, le 12, 13 et 14 juin prochain. Cantine chauffée, restauration chaude, bar et ambiance tout le week-end !!!'
taxonomy:
    category:
        - la-vie-au-ranch
category: la-vie-au-ranch
---

Le 12, 13 et 14 juin 2015, venez fêter avec nous la 24ème de la Saint-Bison. Cantine chauffée, restauration chaude, bar et ambiance tout le week-end !

Au programme:

Vendredi 12 juin:

dès 21:00: Concert de Paul Mac Bonvin


Samedi 13 juin:

10:00: Randonnée à cheval dans Chasseral
dès 21:00: Concert de "The King Bakers Combo"
Dimanche 14 juin:

Maquillages pour les enfants
dès 11:30: Concert avec piano mécanique de 1892 avec son comique, ainsi que les GuggenVal.

[Voir l'affiche de la Saint-Bison](affiche_saint-bison_2015.pdf?target=_blank)