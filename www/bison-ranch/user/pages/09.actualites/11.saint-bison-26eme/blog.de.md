---
title: 'SAINT BISON 26ÈME'
media_order: 'bison.jpg,affiche_saint-bison_2017.pdf'
background_image: bison.jpg
excerpt: 'Le 9, 10 et 11 juin 2015, venez fêter avec nous la 26ème de la Saint-Bison. Cantine chauffée, restauration chaude, bar et ambiance tout le week-end !'
date: '22-05-2017 08:22'
taxonomy:
    tag:
        - saint-bison
        - concert
    category:
        - la-vie-au-ranch
category: la-vie-au-ranch
---

Le 9, 10 et 11 juin 2015, venez fêter avec nous la 26ème de la Saint-Bison. 
Cantine chauffée, restauration chaude, bar et ambiance tout le week-end !

Au programme: 

Vendredi 9 juin: dès 20:30: Concert de Paul Mac Bonvin

Samedi 11 juin: 10:00: Randonnée à cheval dans Chasseral, 18:00 COncert piano mécanique, dès 20:30: Liane Edwards country roc

Dimanche 12 juin: Maquillages pour les enfants dès 11:00: Concert Nivrozic ORVIN, dès 14:00 Concert Liane Edwards

[Voir l'affiche de la Saint-Bison](/user/pages/09.actualites/01.saint-bison-26eme/affiche_saint-bison_2017.pdf?target=_blank)