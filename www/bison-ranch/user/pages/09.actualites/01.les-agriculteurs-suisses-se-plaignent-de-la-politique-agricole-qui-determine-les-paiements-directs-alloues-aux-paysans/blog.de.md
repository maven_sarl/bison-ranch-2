---
title: 'LES AGRICULTEURS SUISSES SE PLAIGNENT DE LA POLITIQUE AGRICOLE QUI DÉTERMINE LES PAIEMENTS DIRECTS ALLOUÉS AUX PAYSANS'
media_order: reportage-tsr.jpg
background_image: reportage-tsr.jpg
excerpt: 'Témoignage de Christian Lecomte sur la politique agricole 2014 - 2017.'
taxonomy:
    category:
        - divers
category: la-vie-au-ranch
---

<a target="_blank" href="http://www.rts.ch/play/tv/le-19h30/video/les-agriculteurs-suisses-se-plaignent-de-la-politique-agricole-qui-determine-les-paiements-directs-alloues-aux-paysans?id=6750458">http://www.rts.ch/play/tv/le-19h30/video/les-agric...</a>

<iframe src="//tp.srgssr.ch/p/rts/embed?urn=urn:rts:video:6750458" width="624" height="351" frameborder="0" name="Les agriculteurs suisses se plaignent de la politique agricole qui détermine les paiements directs alloués aux paysans"></iframe>