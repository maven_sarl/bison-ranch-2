---
title: 'NOUVELLES NAISSANCES'
media_order: '3f90d7ebbf8ab4070594f0ed7ab4af6595a81e2c_bison12.jpg,8ac7954e66e41324c32e143ea17d680c7d4d4959_69377b5784a4921af2a2aec1b321eb50.jpg,76b851f184bdbb016446d7261e704dcb4129e253_bison11.jpg'
background_image: 76b851f184bdbb016446d7261e704dcb4129e253_bison11.jpg
excerpt: 'Le premier bisonneau est né il y a 7 jours. Le second est né aujourd''hui. Tout le monde se porte bien !'
gallery:
    -
        image: 76b851f184bdbb016446d7261e704dcb4129e253_bison11.jpg
    -
        image: 8ac7954e66e41324c32e143ea17d680c7d4d4959_69377b5784a4921af2a2aec1b321eb50.jpg
    -
        image: 3f90d7ebbf8ab4070594f0ed7ab4af6595a81e2c_bison12.jpg
taxonomy:
    tag:
        - bison
        - naissance
    category:
        - la-vie-au-ranch
        - test
category: la-vie-au-ranch
---

Le premier bisonneau est né il y a 7 jours. Le second est né aujourd'hui.

Tout le monde se porte bien !